<?php

namespace DeBear\Helpers\My\Traits\UnitTest;

use DeBear\Helpers\PHPUnit\RefreshDatabase;

trait IncludeRefreshDatabase
{
    /**
     * Store whether we have built the initial backup cache.
     * @var boolean
     */
    protected $cached_db = false;

    /**
     * Ensure we have a valid backup before we start.
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        if (!$this->cached_db) {
            RefreshDatabase::run();
            $this->cached_db = true;
        }
    }

    /**
     * Clean the database between runs.
     * @return void
     */
    public function tearDown(): void
    {
        RefreshDatabase::run();
        parent::tearDown();
    }
}
