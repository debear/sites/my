# My DeBear.uk

A separate site to our other sites for users to manage their accounts, from registration to forgotten passwords and account updates. Does not include processing of logins, which still need to be managed within the skeleton.

## Status

[![Version: 2.0](https://img.shields.io/badge/Version-2.0-brightgreen.svg)](https://gitlab.com/debear/my/blob/master/CHANGELOG.md)
[![Build: 280](https://img.shields.io/badge/Build-280-yellow.svg)](https://gitlab.com/debear/my/blob/master/CHANGELOG.md)
[![Skeleton: 1432](https://img.shields.io/badge/Skeleton-1432-orange.svg)](https://gitlab.com/debear/skeleton)
[![Coding Style: PSR-12](https://img.shields.io/badge/Coding_Style-PSR--12-lightgrey.svg)](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md)
[![Pipeline Status](https://gitlab.com/debear/my/badges/master/pipeline.svg)](https://gitlab.com/debear/my/commits/master)
[![Coverage Report](https://gitlab.com/debear/sites/my/badges/master/coverage.svg)](https://gitlab.com/debear/sites/my/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Motivation

Originally built in to the skeleton, after time some of the older sites were lagging behind and required significant attention to be upgraded any time we made even minor changes to the user account management portal. Splitting this out has allowed us to concentrate separately on user management without having to merge these changes in to all sites immediately.

The mild exception to this is the login function, which remains in the skeleton to avoid logging in through an alternate domain, as this is an undesirable action. Not sure there's anything technical in it - the session and cookie is still configured to be available to all sub-domains - it's more to do with sub-domains that are flashed up during the login process.

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/my/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/my/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
