## Build v2.0.280 - f80fc9a - 2025-02-22 - SoftDep: February 2025
* f80fc9a: Update PHP.ini locations to the new docker image location (_Thierry Draper_)
* 2cc265b: Migrate config getting to the framework Config class (_Thierry Draper_)

## Build v2.0.278 - d176088 - 2025-01-17 - SoftDep: January 2025
* (Auto-commits only)

## Build v2.0.278 - 8e8ede4 - 2024-12-15 - SoftDep: December 2024
* (Auto-commits only)

## Build v2.0.278 - a30b3f1 - 2024-11-16 - SoftDep: November 2024
* (Auto-commits only)

## Build v2.0.278 - fc4bf82 - 2024-10-21 - SoftDep: October 2024
* (Auto-commits only)

## Build v2.0.278 - c44b294 - 2024-09-15 - SoftDep: September 2024
* (Auto-commits only)

## Build v2.0.278 - 6af16e2 - 2024-09-15 - SysAdmin: CI Tweaks
* 6af16e2: Catch Phan run errors that could cause cyclical self-calling (_Thierry Draper_)
* 83ceb73: Fix first run setup for downloading seed data (_Thierry Draper_)
* 3512701: Apply some minor CI script tidying and improvements (_Thierry Draper_)

## Build v2.0.275 - d3216f7 - 2024-08-19 - SoftDep: August 2024
* (Auto-commits only)

## Build v2.0.275 - 1c5d1fe - 2024-08-09 - SysAdmin: MariaDB to MySQL Migration
* 1c5d1fe: Migrate our CI from MariaDB to MySQL (_Thierry Draper_)

## Build v2.0.274 - b644358 - 2024-07-17 - Hotfix: SAST Reporting
* b644358: Fix the SAST report download sort ordering (_Thierry Draper_)

## Build v2.0.273 - 7b8e6e1 - 2024-07-17 - Feature: CI Server Sync Job
* 7b8e6e1: Include the server sync database to our CI job list (_Thierry Draper_)

## Build v2.0.272 - 78b7235 - 2024-07-16 - SoftDep: July 2024
* (Auto-commits only)

## Build v2.0.272 - ba54874 - 2024-06-21 - Hotfix: Local CI Job Parsing
* ba54874: Fix the local CI job parser to exclude comments after the allow_failure flag (_Thierry Draper_)

## Build v2.0.271 - 31391cc - 2024-06-18 - SoftDep: June 2024
* (Auto-commits only)

## Build v2.0.271 - b369fd9 - 2024-06-17 - Hotfix: CI Downloads
* b369fd9: Fix the request sent to determine what CI data files need downloading (_Thierry Draper_)
* aa6ff4b: Improve the location of the CI download trust store cert (_Thierry Draper_)

## Build v2.0.269 - f4ee292 - 2024-05-18 - SoftDep: May 2024
* (Auto-commits only)

## Build v2.0.269 - eb70871 - 2024-05-13 - Security: CI Jobs
* eb70871: Switch our localci SAST exclusion to the new emulator (_Thierry Draper_)
* e5b7426: Implement a local SAST emulator script using the GitLab docker images (_Thierry Draper_)
* 35b2184: Remove legacy homebrew image files that are no longer in use (_Thierry Draper_)
* dfd14d6: Implement an initial SAST reporting tool (_Thierry Draper_)
* 33cbfeb: Migrate our only/except job logic to the preferred rules format (_Thierry Draper_)
* 83bb2b0: Incorporate the GitLab managed SAST pipeline job (_Thierry Draper_)
* afd4427: Add some native tool dependency vulnerability scanning within our CI (_Thierry Draper_)

## Build v2.0.262 - 2351170 - 2024-04-27 - Improvement: CI Downloads
* 2351170: Switch CI downloads from wget to curl (_Thierry Draper_)

## Build v2.0.261 - 5972933 - 2024-04-24 - Improvement: JavaScript Formatting
* 5972933: Apply some minor eslint hint tweaks to the JavaScript (_Thierry Draper_)
* 344e1e3: Disable the eslint unused disabled rule checked whilst linting, as it conflicts with the standards check (_Thierry Draper_)
* 39d999f: Migrate to eslints flat config format (_Thierry Draper_)

## Build v2.0.258 - aa96b97 - 2024-04-21 - SoftDep: April 2024
* (Auto-commits only)

## Build v2.0.258 - e458fbe - 2024-04-18 - Improvement: CI ECMAScript Version
* e458fbe: Update the targeted version of ECMAScript in CI to v13 / 2022 (_Thierry Draper_)

## Build v2.0.257 - 6238a73 - 2024-03-17 - SoftDep: Laravel 11 and March 2024 Update
* 6238a73: Upgrade dependencies to PHPUnit 11 (_Thierry Draper_)
* 288bc65: Upgrade dependencies to PHPUnit 10 (_Thierry Draper_)

## Build v2.0.255 - b120ddf - 2024-02-17 - SoftDep: February 2024
* (Auto-commits only)

## Build v2.0.255 - eda682a - 2024-01-27 - Hotfix: New Password Validation
* eda682a: Fix the naming of the new password validation methods (_Thierry Draper_)

## Build v2.0.254 - 747dce2 - 2024-01-27 - Hotfix: Password Policy Wording
* 747dce2: Fix a typo in the password policy wording (_Thierry Draper_)

## Build v2.0.253 - 5df8611 - 2024-01-27 - Improvement: Refactoring JavaScript
* 5df8611: Apply the latest round of micro-optimisations and small improvements (_Thierry Draper_)
* 1b6f1d2: Revert PHPCS "linting" enforced JavaScript standards for curly brace positioning (_Thierry Draper_)
* f035816: Replace the JavaScript linter with something less enforcing of (clunky) standards (_Thierry Draper_)
* e18907d: Move to greater use of the arrow functions (_Thierry Draper_)
* 635f6ca: Make better use of template literals in string expressions (_Thierry Draper_)
* c094034: Move away from the deprecated centralised DOM querySelector parent node argument (_Thierry Draper_)
* c6fdc0f: Replace use of the deprecated DOM.get* methods in favour of the generalised query selector (_Thierry Draper_)
* ecf3875: Move registration form JavaScript events from server-side (rendered in the HTML) to client-side (_Thierry Draper_)

## Build v2.0.245 - 70a73ce - 2024-01-19 - Improvement: JavaScript Null Operators
* 70a73ce: Update eslints base ECMAScript version to allow for null-handling simplification (_Thierry Draper_)

## Build v2.0.244 - 4261d02 - 2024-01-14 - SoftDep: January 2024
* (Auto-commits only)

## Build v2.0.244 - 1c0e713 - 2023-12-17 - SoftDep: December 2023
* (Auto-commits only)

## Build v2.0.244 - 46e7e89 - 2023-11-27 - CI: Standardised CSS Standards
* 46e7e89: Apply our standard 120-char width for CSS standards rather than the default 80 (_Thierry Draper_)

## Build v2.0.243 - 6769cc9 - 2023-10-14 - Composer: October 2023
* 6769cc9: Backport a CI fix from earlier skeleton CDN update (_Thierry Draper_)

## Build v2.0.242 - a94b900 - 2023-09-18 - Composer: September 2023
* (Auto-commits only)

## Build v2.0.242 - 3763b88 - 2023-08-30 - Improvement: CI SymLink Conversion
* 3763b88: Switch to the new symlink-to-dir CI converter (_Thierry Draper_)

## Build v2.0.241 - a73e9e0 - 2023-08-05 - Composer: August 2023
* (Auto-commits only)

## Build v2.0.241 - b60291b - 2023-07-15 - Composer: July 2023
* (Auto-commits only)

## Build v2.0.241 - a161e07 - 2023-07-13 - Hotfix: Activity Log CI
* a161e07: Switch the activity log type CI test to use the new numeric value, instead of label-based (_Thierry Draper_)

## Build v2.0.240 - a224420 - 2023-06-17 - Composer: June 2023
* (Auto-commits only)

## Build v2.0.240 - b4b7948 - 2023-05-14 - Composer: May 2023
* (Auto-commits only)

## Build v2.0.240 - 37775b3 - 2023-05-02 - SysAdmin: Deprecations
* 37775b3: Replace use of fgrep with grep given it is being flagged overtly as obsolete (_Thierry Draper_)

## Build v2.0.239 - 9858752 - 2023-04-15 - Composer: April 2023
* (Auto-commits only)

## Build v2.0.239 - a55f12d - 2023-03-11 - Composer: March 2023
* (Auto-commits only)

## Build v2.0.239 - d5065bc - 2023-03-01 - Improvement: CSS Standards to Prettier
* d5065bc: Fix CSS standards according to the new Prettier rules (_Thierry Draper_)
* 52cef16: Switch the deprecated Stylelint CSS standards to Prettier (_Thierry Draper_)

## Build v2.0.237 - da07c93 - 2023-02-12 - Composer: February 2023
* (Auto-commits only)

## Build v2.0.237 - 7e00fff - 2023-01-15 - Composer: January 2023
* (Auto-commits only)

## Build v2.0.237 - cc4a7d5 - 2023-01-08 - SysAdmin: CI Docker Images from Container Registry
* cc4a7d5: Switch the CI jobs to images from our Container Registry (_Thierry Draper_)

## Build v2.0.236 - e289c36 - 2023-01-05 - Improvement: MySQL Linting
* e289c36: Switch the CI MariaDB collation to latin1_general (_Thierry Draper_)
* 8aebe8e: Include the CI job for linting setup scripts (_Thierry Draper_)

## Build v2.0.234 - f1b4e72 - 2022-12-18 - Composer: December 2022
* (Auto-commits only)

## Build v2.0.234 - b94296d - 2022-11-19 - Composer: November 2022
* (Auto-commits only)

## Build v2.0.234 - 884e4a1 - 2022-10-16 - Composer: October 2022
* (Auto-commits only)

## Build v2.0.234 - f0674f5 - 2022-09-18 - Composer: September 2022
* (Auto-commits only)

## Build v2.0.234 - 06b0d33 - 2022-09-15 - CI: Environment dependency fix
* 06b0d33: Install wget as a CI environment dependency (_Thierry Draper_)

## Build v2.0.233 - 989da93 - 2022-08-14 - Composer: August 2022
* (Auto-commits only)

## Build v2.0.233 - 7a50707 - 2022-07-19 - SysAdmin: Backport for PHP 8.0
* 7a50707: Update the composer file to allow PHP 8.0, as well as the previous 8.1 (_Thierry Draper_)

## Build v2.0.232 - 702d0d3 - 2022-07-16 - Composer: July 2022
* (Auto-commits only)

## Build v2.0.232 - 6f632e8 - 2022-07-15 - Hotfix: PHPMD TooManyMethods whitelist
* 6f632e8: Increase the scope of PHPMDs TooManyMethods whitelist (_Thierry Draper_)

## Build v2.0.231 - 79597d4 - 2022-06-19 - Composer: June 2022
* (Auto-commits only)

## Build v2.0.231 - f642410 - 2022-06-08 - SysAdmin: Revert to native Blade data displaying
* f642410: Re-factor our Blade template variable echoing to use the appropriate native Laravel mechanism (_Thierry Draper_)
* 40260d8: Move common DeBear helper classes to the alias list for use in Blade templates (_Thierry Draper_)
* ae48f23: Revert use of @print and @config in Blade templates with the native Laravel method (_Thierry Draper_)
* ed92a98: Restore Blade linting using the new dependency (_Thierry Draper_)

## Build v2.0.227 - 0bf2a1e - 2022-05-27 - CI: PHP Code Coverage streamlining
* 0bf2a1e: Streamline the PHP Code Coverage output, and consider output less than 100% as a failure (_Thierry Draper_)

## Build v2.0.226 - 742fca9 - 2022-05-26 - CI: Report PHP Unit code coverage on master
* 742fca9: Propagate the PHP Unit code coverage report to the deploy stage for its result to be tagged against the master branch (_Thierry Draper_)

## Build v2.0.225 - 227dcc0 - 2022-05-22 - CI: Switch Code Coverage Driver
* 227dcc0: Switch Code Coverage from using XDebug to PCOV (_Thierry Draper_)

## Build v2.0.224 - b7fe35f - 2022-05-22 - Improvement: Unit Tests as part of Code Coverage
* b7fe35f: Add the GitLab code coverage regexp pattern, previously configured within the UI (_Thierry Draper_)
* 4fbce62: Refine our Unit Tests according to localci tweaks made to the skeleton (_Thierry Draper_)
* 72491dd: Include appropriate PHPUnit code in our code coverage tests (_Thierry Draper_)

## Build v2.0.221 - b29fe93 - 2022-05-22 - Improvement: Laravel 9
* b29fe93: Fix the user validation parsing identified by the Laravel 9 upgrade (_Thierry Draper_)
* 9d856ff: Remove Blade linting that is no longer available following the Laravel 9 upgrade (_Thierry Draper_)

## Build v2.0.219 - 1b4622d - 2022-05-21 - Improvement: Registration form Terms default
* 1b4622d: Stop the registration form's default/current state of the terms field being fixed (_Thierry Draper_)

## Build v2.0.218 - a19e73b - 2022-05-21 - Improvement: Email app definition
* a19e73b: Make the corresponding changes for standardising the email app in its template (_Thierry Draper_)

## Build v2.0.217 - f63e837 - 2022-05-21 - SysAdmin: PHP 8
* ef9dc71: Use a new stylelint rule modifier to prevent recent false negatives (_Thierry Draper_)
* 1b144ea: Add a new CI step to lint Blade templates (_Thierry Draper_)
* 06d94d2: Move away from our Arrays::hasIndex wrapper (_Thierry Draper_)
* 86f018b: Implement PHP 8s new union param/return types (_Thierry Draper_)
* cedb2ac: Include a repo-specific version of the Phan PHP Standards CI job (_Thierry Draper_)
* d744eed: Apply several type-hinting related fixes (_Thierry Draper_)
* 4d8467c: Remove unused imported classes (_Thierry Draper_)
* e08836e: Switch from Laravel's on-the-fly class facades at the root level to their actual full path (_Thierry Draper_)
* 5ea87a9: Include mixed class method paramter type hints from PHP 8 (_Thierry Draper_)

## Build v2.0.208 - d89326c - 2021-11-13 - Composer: November 2021
* (Auto-commits only)

## Build v2.0.208 - 57adcdf - 2021-11-12 - CI: CSS Property Standards
* 57adcdf: Update CSS property rules to reflect the new alphabetical standards (_Thierry Draper_)
* f097866: Add new stylelint standards test for alphabetical CSS property ordering (_Thierry Draper_)
* b76b903: Remove a recently deprecated stylelint rule (_Thierry Draper_)

## Build v2.0.205 - 74613c7 - 2021-10-16 - Composer: October 2021
* (Auto-commits only)

## Build v2.0.205 - 5540fd3 - 2021-09-18 - Composer: September 2021
* (Auto-commits only)

## Build v2.0.205 - a8571dc - 2021-08-13 - Composer: August 2021
* (Auto-commits only)

## Build v2.0.205 - 6cdaf09 - 2021-08-13 - Hotfix: PHPUnit XML .env spec
* 6cdaf09: Add missing .env values to the PHPUnit test XML spec (_Thierry Draper_)

## Build v2.0.204 - 72b31a5 - 2021-07-27 - Feature: Sitemap URL tester
* 72b31a5: Sitemap parser and processor for URL testing (_Thierry Draper_)

## Build v2.0.203 - 07f305b - 2021-07-17 - Composer: July 2021
* (Auto-commits only)

## Build v2.0.203 - 64a834a - 2021-06-21 - Composer: June 2021
* (Auto-commits only)

## Build v2.0.203 - 5be4e5e - 2021-05-15 - Composer: May 2021
* (Auto-commits only)

## Build v2.0.203 - d84ee97 - 2021-04-30 - CI: Pipeline Tweaks
* d84ee97: Make use of the CI's needs: option to streamline job and stage links (_Thierry Draper_)

## Build v2.0.202 - 71c997b - 2021-04-17 - Composer: April 2021
* (Auto-commits only)

## Build v2.0.202 - 42a00ed - 2021-03-13 - Composer: March 2021
* (Auto-commits only)

## Build v2.0.202 - 4e16c59 - 2021-02-13 - Composer: February 2021
* (Auto-commits only)

## Build v2.0.202 - 912e2a9 - 2021-01-16 - Composer: January 2021
* (Auto-commits only)

## Build v2.0.202 - fa7a31b - 2021-01-11 - Feature: Password Policy
* fa7a31b: Handle users redirected to the password reset page when their password does not comply with the policy (_Thierry Draper_)
* 53101a3: Enforce a new password policy for new registrations (_Thierry Draper_)

## Build v2.0.200 - 31cbbdf - 2021-01-03 - CI: Only deploy on master branch
* 31cbbdf: Only deploy during CI/AD of the master branch (_Thierry Draper_)

## Build v2.0.199 - 1794720 - 2020-12-20 - Composer: December 2020
* (Auto-commits only)

## Build v2.0.199 - 12f04cc - 2020-12-05 - Improvement: Move to Xdebug 3
* 12f04cc: Fixes for the switch from Xdebug 2 to 3 (_Thierry Draper_)

## Build v2.0.198 - b81e21b - 2020-11-14 - Composer: November 2020
* (Auto-commits only)

## Build v2.0.198 - 0df43f8 - 2020-10-16 - Improvement: Unit Test manifest.json
* 0df43f8: Add the manifest.json file to the suite of Unit Tests (_Thierry Draper_)

## Build v2.0.197 - 9c4d3db - 2020-10-09 - Composer: Upgrade to Laravel 8
* 9c4d3db: Update our CI rules following the Laravel 8 (+deps) upgrade (_Thierry Draper_)
* c5a52f6: Update dependencies from upgrading from Laravel 7 to 8 (_Thierry Draper_)

## Build v2.0.195 - 5066616 - 2020-09-26 - Improvement: Migrations to Schema
* 5066616: Switch from Migrations to Schemas updated via schema-sync (_Thierry Draper_)

## Build v2.0.194 - 8e29527 - 2020-09-19 - Composer: September 2020
* (Auto-commits only)

## Build v2.0.194 - 036c13a - 2020-09-19 - CI: Merge PHP Standards Jobs
* 036c13a: Minor CI Code Coverage improvements (_Thierry Draper_)
* fa570cc: Merge the PHP Mess Detector and Standards tests into a single job (_Thierry Draper_)

## Build v2.0.192 - 86ad410 - 2020-09-18 - Improvement: HTTP Status Wrapper
* 86ad410: Start using generic (named) wrappers for sending particular HTTP status codes (_Thierry Draper_)

## Build v2.0.191 - 56c3b9b - 2020-08-15 - Composer: August 2020
* (Auto-commits only)

## Build v2.0.191 - c70fd0e - 2020-07-13 - Composer: July 2020
* (Auto-commits only)

## Build v2.0.191 - f219bab - 2020-06-17 - Hotfix: PHPUnit Test Locale
* f219bab: Fix PHPUnit tests on CI as the locale does not match dev/prod environments (_Thierry Draper_)

## Build v2.0.190 - 1d47b39 - 2020-06-14 - Composer: June 2020
* (Auto-commits only)

## Build v2.0.190 - 97f1542 - 2020-06-13 - SysAdmin: Reduce Release Overlap
* 97f1542: Reduce the overlap time between releases when performing the release (_Thierry Draper_)

## Build v2.0.189 - 1b1c71a - 2020-06-06 - Improvement: PSR-4-like Standards Check
* 1b1c71a: Add PSR-4 like standards checks, given recent class and file name mis-matches (_Thierry Draper_)

## Build v2.0.188 - 8e1d1e8 - 2020-06-02 - CI: Local PHPUnit Dev Setup
* 8e1d1e8: Add scripts to setup a local dev environment for working on PHPUnit tests (_Thierry Draper_)

## Build v2.0.187 - fad9f53 - 2020-05-18 - Composer: May 2020
* (Auto-commits only)

## Build v2.0.187 - 0784ac1 - 2020-04-21 - Composer: April 2020
* (Auto-commits only)

## Build v2.0.187 - 994f7bb - 2020-04-15 - Improvement: 100% Code Coverage
* 994f7bb: Add the Form helpers to the Code Coverage, with unit tests to get us to 100% coverage (_Thierry Draper_)
* b1ae4d2: Expand the PHPUnit tests to 100% code coverage of the controllers (_Thierry Draper_)

## Build v2.0.185 - 38d3c93 - 2020-04-07 - Improvement: Dynamic PHPUnit Setup Components
* 38d3c93: Standarise and split the PHPUnit setup script into dynamic components (_Thierry Draper_)

## Build v2.0.184 - d1a93e6 - 2020-04-06 - Hotfix: PHPUnit Error Handling
* d1a93e6: Ensure the new PHPUnit run script errors on failure at the appropriate stage (_Thierry Draper_)

## Build v2.0.183 - e357dd0 - 2020-04-04 - Composer: Replacing PHP Linter
* e357dd0: Replace a deprecated PHP Linting package with its replacement (_Thierry Draper_)

## Build v2.0.182 - 9a89cbb - 2020-04-04 - Improvement: PHPUnit Efficiency
* 9a89cbb: Split the PHPUnit run in to a more efficient process (_Thierry Draper_)

## Build v2.0.181 - 43c4efb - 2020-03-14 - Composer: March 2020
* (Auto-commits only)

## Build v2.0.181 - d9b5472 - 2020-02-14 - Composer: February 2020
* (Auto-commits only)

## Build v2.0.181 - 6b0f7b8 - 2020-01-31 - SysAdmin: .gitignore for env config
* 6b0f7b8: Updated gitignore to hide a local env config file (_Thierry Draper_)

## Build v2.0.180 - 12ff77d - 2020-01-23 - SysAdmin: Session Hits Table Removal Fallout
* 12ff77d: Fallout from removing the session hits table (_Thierry Draper_)

## Build v2.0.179 - 5fc29c8 - 2020-01-12 - Composer: January 2020
* (Auto-commits only)

## Build v2.0.179 - 75b5288 - 2019-12-26 - Hotfix: PHPUnit CI Job Shell Errors
* 75b5288: Fix tput/clear errors during the PHPUnit CI job (_Thierry Draper_)

## Build v2.0.178 - 201e95e - 2019-12-15 - Composer: December 2019
* (Auto-commits only)

## Build v2.0.178 - 09d3f9c - 2019-11-19 - CI: PHP Comments Standards
* 09d3f9c: Fall-out from the new PHP comments standards being enforced (_Thierry Draper_)
* ae28425: Extend our PSR-12 standards check to include the format of comments (_Thierry Draper_)

## Build v2.0.176 - a81ad2c - 2019-11-16 - Composer: November 2019
* (Auto-commits only)

## Build v2.0.176 - 94e75a5 - 2019-11-14 - CI: YAML Fixes and Improvements
* 94e75a5: CI YAML fixes and improvements, such as moving the PHP/MySQL versions used to GitLab (_Thierry Draper_)

## Build v2.0.175 - ce6291b - 2019-11-05 - Composer: Laravel 6 Fallout
* ce6291b: Switch from using the Input:: facade to Request::, as it was removed in Laravel 6 (_Thierry Draper_)

## Build v2.0.174 - 82adae3 - 2019-10-23 - Composer: October 2019
* 82adae3: PSR-12 whitespace fixes (_Thierry Draper_)

## Build v2.0.173 - f55dbee - 2019-10-20 - Improvement: CSS Linting
* f55dbee: CSS tweaks from new linting and standards checks (_Thierry Draper_)
* cad55c9: Switch the CSS Linting test to use stylelint, and split in to both linting and standards (_Thierry Draper_)

## Build v2.0.171 - cdc21c3 - 2019-09-25 - Composer: September 2019
* (Auto-commits only)

## Build v2.0.171 - b172ebd - 2019-09-02 - CI: Fix 'Clean' Local Runs
* b172ebd: Fix the 'clean' running of our CI locally (_Thierry Draper_)

## Build v2.0.170 - b11e630 - 2019-08-27 - Composer: August 2019
* b11e630: Fall-out from recent PHPMD to standards (_Thierry Draper_)

## Build v2.0.169 - 974998a - 2019-08-09 - CD: Move Server Details to Env Vars
* 974998a: Move the deploy server access details to environment variables (_Thierry Draper_)

## Build v2.0.168 - c601264 - 2019-07-21 - Composer: July 2019
* (Auto-commits only)

## Build v2.0.168 - 6ebe7e3 - 2019-07-17 - Feature: Monitor PHP Code Coverage
* 6ebe7e3: Enable PHP code coverage report during CI (_Thierry Draper_)

## Build v2.0.167 - 68b627c - 2019-07-10 - Hotfix: Skip Deploy in LocalCI
* 68b627c: The new deploy stage should not be included in our localci run script (which also requires the script: rule to be last) (_Thierry Draper_)

## Build v2.0.166 - 03cf07d - 2019-07-05 - Feature: Enable Continuous Delivery
* 03cf07d: Envoy script for implementing our Continuous Delivery (_Thierry Draper_)

## Build v2.0.165 - 6bd6fdd - 2019-06-18 - CI: PHPUnit vendor simplification
* 6bd6fdd: May need to create the base CPAN CI dir (_Thierry Draper_)
* 5a5b407: Fix, and where appropriate merge, vendor folder creation within the CI (_Thierry Draper_)
* f262cb7: Installing the skeleton for CI is a simpler process (_Thierry Draper_)
* 99a3191: Ensure composer has a vendor folder to be installed into (_Thierry Draper_)
* 0afde81: Propagate the CI_FAUX flag when running CI tests locally, but from clean (_Thierry Draper_)

## Build v2.0.160 - 8f12299 - 2019-06-10 - Hotfix: Flag CSS
* 8f12299: Remove skeleton CSS bodge for flag vertical-align that has been fixed in the skeleton (_Thierry Draper_)

## Build v2.0.159 - 0208b28 - 2019-06-09 - SysAdmin: Sprites to CDN
* 0208b28: Move the sprites to the CDN (_Thierry Draper_)

## Build v2.0.158 - 34038c5 - 2019-06-08 - CI: Handle Perl Upgrades
* 34038c5: Handle changing versions of perl in the CI (_Thierry Draper_)

## Build v2.0.157 - 0c6215d - 2019-06-07 - CI: Data Download Errors
* 0c6215d: When downloading test CI data return an error if the download fails, rather than assume it was a 204/skip (_Thierry Draper_)

## Build v2.0.156 - 276f5cd - 2019-05-21 - CI: Fix PHPUnit skeleton setup
* 276f5cd: rsync management of PHPUnit test setup fix (_Thierry Draper_)

## Build v2.0.155 - 1928586 - 2019-05-02 - Migrations: Merge w/Stored Procedures
* 1928586: Re-organise the database migrations layouts to keep all sub-grouped migrations/stored procs together (_Thierry Draper_)

## Build v2.0.154 - b98ba5e - 2019-04-22 - CI: Testing Migrations
* b98ba5e: Include running the database migrations properly as part of the MySQL Linting CI job (plus testing their rollbacks) (_Thierry Draper_)

## Build v2.0.153 - 5b05a86 - 2019-04-16 - Composer: April 2019 updates
* 5b05a86: Some local 'clean' CI test fixes and code optimisations (_Thierry Draper_)

## Build v2.0.152 - 909626c - 2019-04-09 - CI: Fix Logging
* 909626c: Automatically fix the logs when cloning the skeleton within CI (_Thierry Draper_)

## Build v2.0.151 - 355e975 - 2019-04-08 - CI: Check Migrations
* 355e975: Re-jig how we run Laravel's migrations script should we sub-filter our migrations (_Thierry Draper_)

## Build v2.0.150 - f37b7b4 - 2019-03-29 - CI: Exclude Tags
* f37b7b4: Prevent CI when pushing tags (_Thierry Draper_)

## Build v2.0.149 - d5dfbb3 - 2019-03-26 - CI: Re-grouped Git remote
* d5dfbb3: Reflect re-grouping of the git remotes in our CI scripts (_Thierry Draper_)

## Build v2.0.148 - c1df62d - 2019-03-22 - CI: MySQL Linter
* c1df62d: Minor changes as part of importing a MySQL linter (_Thierry Draper_)
* a185487: Standardising the CHANGELOG to our new format (_Thierry Draper_)

## Build v2.0.146 - f73f4a6 - 2019-03-09 - Hotfix: Post Launch
* f73f4a6: Start using a singular composer cache to try and cut down on CI pipeline build time (_Thierry Draper_)
* eb1dc76: Revise the way test databases are created, and include some post-test cleanup of them (_Thierry Draper_)
* c05d295: CI refinements to the Composer vendor caching rules and include a new Perl-linting process (_Thierry Draper_)
* 30275db: Make the unit tests compatible with some skeleton filesystem tweaks (_Thierry Draper_)
* 96a5867: Test script tweaks to attempt repo-name parity between repo and skeleton (_Thierry Draper_)
* 6f5c706: Use the correct $_SERVER variable to check domain without considering port (_Thierry Draper_)
* d08d26f: Root-level merged folder isn't actually required (_Thierry Draper_)
* 2b91379: Fix incorrect $_SERVER variable name used on production (_Thierry Draper_)
* 8e0f6ac: Fix the vertical alignment of the timezone country dropdown (_Thierry Draper_)
* 4d6cab6: Add symlinks required from live (_Thierry Draper_)

## Build v2.0.136 - 75b5dea - 2019-02-17 - Launching Laravel - [![Version: 2.0](https://img.shields.io/badge/Version-2.0-brightgreen.svg)](https://gitlab.com/debear/my/tree/v2.0)
* 75b5dea: Git tidy pre merge of Laravel branch (_Thierry Draper_)
* dbb4380: Make the VERSION info part of the repo (_Thierry Draper_)
* c439224: Minor CI tidying (_Thierry Draper_)
* 9af6c93: Minor controller file tidying (_Thierry Draper_)
* 7203155: The old db folder contains legacy backups, so nuke for now (_Thierry Draper_)
* 67e583c: PHPUnit setup script does need moving up in the CI YML (_Thierry Draper_)
* 66fbf45: Prune old MySQL seed data, plus build schema from migrations need downloaded seed (_Thierry Draper_)
* d9d5f09: Regular Composer update (_Thierry Draper_)
* ef8b636: Minor fixes picked out by the Unit Tests (_Thierry Draper_)
* 0a8fe48: Implement a "feature" PHPUnit test, testing access to the homepage (_Thierry Draper_)
* 39551e5: Refine the way sub-site routes are loaded (_Thierry Draper_)
* d68875c: CI composer fix to only install missing/out-of-date packages rather than update outside of composer.lock context (_Thierry Draper_)
* 5df621d: Fix My routes following PSR tweaks (that weren't picked up earlier) (_Thierry Draper_)
* 23c7a64: Remove eslint CI overcomplication... just run eslint in our eslint Docker! (_Thierry Draper_)
* bd3b6ae: Fixes from the new JS standards test (_Thierry Draper_)
* ac726c9: Include extended JS standards checking in the CI (_Thierry Draper_)
* eb72356: Apply TitleCase notation to JavaScript classes and camelCase notation to JavaScript methods (_Thierry Draper_)
* 67da010: Script to run the CI tests locally (and accessing the composer libs directly, rather than through a git-runner) (_Thierry Draper_)
* 308a774: Add PSR and Pipeline Status flags to the README (_Thierry Draper_)
* 07303a0: Password-entered stamping method needs to be called statically (_Thierry Draper_)
* 5804fa4: Fallout from applying PSR-12 to the skeleton (_Thierry Draper_)
* 7f0e387: Add PSR-12 compliance testing to our CI (_Thierry Draper_)
* 904dabb: Fix some JS refactor inconsistencies from the phpcbf run due to subtleties of how we wrote our code (Take II) (_Thierry Draper_)
* d06db34: Fix some JS refactor inconsistencies from the phpcbf run due to subtleties of how we wrote our code (_Thierry Draper_)
* 17cd67e: Minor PSR tidying following a phpcs run (Part II) (_Thierry Draper_)
* 43eb535: Minor PSR tidying following a phpcs run (_Thierry Draper_)
* 422427c: JS linting fixes using phpcbf (_Thierry Draper_)
* 3332caa: Add some CSS and JS linting to our CI (_Thierry Draper_)
* 892d05e: PHPMD definition tweaks (_Thierry Draper_)
* 97831d3: Linting fix for use of duplicate classnames within a class (_Thierry Draper_)
* 406ee1b: Switch PHP we are testing against from 7.3 to 7.2 (_Thierry Draper_)
* 3d7dea0: Initial project CI (_Thierry Draper_)
* 5e5c6f6: Record a user has just entered their password when updating their password (via either the My Account page or Password Reset) (_Thierry Draper_)
* 32e2541: Flag the label field in red when a form field fails validation (_Thierry Draper_)
* b1e1369: My Account form (_Thierry Draper_)
* ccb1835: Prevent flagging actual / secondary errors when form validation fails due to suspicious activity (_Thierry Draper_)
* e7dc805: Minor rename of the session model classes (_Thierry Draper_)
* 7edfe54: Remove legacy password reset columns/questions that we no longer use (_Thierry Draper_)
* 060b1e2: Password reset process, including commonolising some of the registration elements that were also required (_Thierry Draper_)
* 9bdc792: As part of the password reset, seperate the verification process from the old Account controller to keep each process separate and concise (_Thierry Draper_)
* 0d69067: Login details form completion view, some minor associated copy tweaks (_Thierry Draper_)
* 796e53c: Minor Blade template location switch for the verification process (which isn't really part of the registration process) (_Thierry Draper_)
* 38176d0: Move the form policy checks from the Controller to the form config (as it breaks complete templates) (_Thierry Draper_)
* 873ed38: Adapt the forms to the new Post/Redirect/Get structure that allows better use of the form complete template (_Thierry Draper_)
* 7e66b72: Minor streamlining of the Register Help toggling JS (_Thierry Draper_)
* a36f1f1: Fix a change in an AJAX request HTTP method that required a change in argument sending mechanism (_Thierry Draper_)
* 1abd05b: Implement the Login Details email trigger, with some standardisation (and minor fixes) from the Registration form (_Thierry Draper_)
* 8e863e1: Some minor URL re-jigs: verification now falls under "my account" and my./my-account/ is now my./account/ (_Thierry Draper_)
* a0c4dba: Handling re-sending of the email verification email from in page (_Thierry Draper_)
* 298e34a: Set the flag to ensure emails are sent to unverified users during the registration and verification stages... when they've not yet had the chance to verify (_Thierry Draper_)
* a61874a: Fix the verification complete controller to bounce to the registration page if the user is not logged in (as opposed to returning a 403) (_Thierry Draper_)
* b6bdd89: Record logins from registration as their own login source (_Thierry Draper_)
* 796459a: Minor CSS specifity fix for the debug panel on the registration view CSS (_Thierry Draper_)
* 3c6af00: Rather than presenting the user with a standard 400 message when verifying a previously verified email, provide something a bit more useful (but preserving the 400 HTTP status) (_Thierry Draper_)
* 9e3e022: User email address verification process (simplified from the legacy version) (_Thierry Draper_)
* d4b023c: Enforce policy checks on the various registration steps (_Thierry Draper_)
* 1f697bf: Simplify how we can access the session handler (_Thierry Draper_)
* bb5f2f7: Include link to where the user came from when completing registration (_Thierry Draper_)
* 197c54c: Implement the "Registration Complete" view (_Thierry Draper_)
* d946aa1: Move the Registration and Login Details controllers in to their own classes and from the skeleton (_Thierry Draper_)
* 1c82686: Fix missing config option in password length on registration form validation (_Thierry Draper_)
* 850cd32: Responding the Registration form (_Thierry Draper_)
* 5c05276: Minor LICENSE file tidying (_Thierry Draper_)
* bf4514d: Remove no longer used naming convention (_Thierry Draper_)
* ac87abd: Streamline the icon sprite (_Thierry Draper_)
* e745b4a: Tweaked iconset CSS naming (_Thierry Draper_)
* 2465c0d: Updated iconset for the nav changes (_Thierry Draper_)
* f4af842: Move the config away from "GA" specifically to a more generic term (_Thierry Draper_)
* c3dbcf3: Standardise the Sitemap and Nav links to the new model (_Thierry Draper_)
* b12c19d: Removed legacy symlink for the meta image that we have improved the implementation of (_Thierry Draper_)
* a269703: Tweaks following recent skeleton CSS updates (_Thierry Draper_)
* 6b9c5af: Re-worked footer link disabling (_Thierry Draper_)
* 798c666: Tweaks based on recent refinements to the skeleton CSS (_Thierry Draper_)
* a951913: New way of excluding the Home/Sitemap links in the bottom footer (_Thierry Draper_)
* 76a5fc2: Tweaked icon sprite (_Thierry Draper_)
* 494166e: Minor PSR standardisation of class names that hadn't been adjusted correctly from the legacy classes (_Thierry Draper_)
* 12c2aa9: Standardise the User model with the naming convention that we're starting to follow (_Thierry Draper_)
* f33b9bd: Include the emails we need to send in the registration process (_Thierry Draper_)
* 96fac70: Turns out you can declare a void return type on methods (_Thierry Draper_)
* 822ae47: Suspicious Activity tracking within the Registration form (_Thierry Draper_)
* 623d218: Re-jig of the forms helper class naming structure, as the class is required for more than just validating (_Thierry Draper_)
* e2c378d: Registration form field mappings, plus some minor fake tweaks (_Thierry Draper_)
* ad18967: Client-side User ID validation on the registration form (_Thierry Draper_)
* 1ba8f72: Registration form server-side validation (_Thierry Draper_)
* ac4d52a: Include default (previous) values in registration form (primarily sessioned data in this instance, but logic applies for other forms too) (_Thierry Draper_)
* a259af3: Include relevant config for Registration form faker details (_Thierry Draper_)
* 188972f: Move form validation attributes to config (_Thierry Draper_)
* 27ea099: Over-rideable form field 'required' tag for testing (_Thierry Draper_)
* 34dae6f: JavaScript form validation for the registration form (_Thierry Draper_)
* 32af3bf: Homepage redirect (missing actual logic for now) to go to registration or accout details form depending on whether the user is logged in or not (_Thierry Draper_)
* 0f0e402: Registration form JavaScript validation (_Thierry Draper_)
* 2434a6a: Effect of splitting out the DOM class into smaller components (_Thierry Draper_)
* 4505d49: Changes due to the refactor of skeleton JS libs (_Thierry Draper_)
* 84c99e7: Finished JS class method name standardisation (_Thierry Draper_)
* 18b92b6: Initial display version of the Registration form (_Thierry Draper_)
* d646db4: Initial setup of Laravel environment (_Thierry Draper_)
* 842001a: Base resources and config from www setup work (_Thierry Draper_)
* 6ae4ff5: Proof-of-concept route (_Thierry Draper_)
* d9cbaf5: Start of process of switching from Homebrew skeleton to Laravel (_Thierry Draper_)
* ea74503: Removal of the 'hosted dev' environment on production (_Thierry Draper_)

## Build v1.0.33 - 35bc064 - 2018-06-10 - Homebrew
* _Changelog truncated for initial build_
