#!/bin/bash
# Import the test database, including acquiring the data from the (remote) test source

dir_root=$(realpath $(dirname $0))
dir_base=$(realpath $dir_root/../..)
dir_scripts=$(realpath $dir_root/../_scripts)
dir_vendor="$dir_base/vendor"

. $dir_scripts/helpers.sh

git_repo="$1"
git_branch="$2"

# Ensure we have repo values
if [ -z "$git_repo" ]
then
    git_repo=$($dir_scripts/git.sh repo)
fi
if [ -z "$git_branch" ]
then
    git_branch=$($dir_scripts/git.sh branch)
fi

# Validation: as this is for testing, we _MUST_ have a DB_PREFIX set!
if [ -z $DB_PREFIX ]
then
    echo "** Missing required environment variable: DB_PREFIX." >&1
    echo "**  Will not run without it, as it may affect production/development databases." >&1
    exit 9
fi

# Seed the database(s) with some base test data
dir_data="$dir_vendor/debear/setup/mysql/$git_repo"
mkdir -p $dir_data
mysql_args="-s --host=$DB_HOST --port=$DB_PORT --user=$DB_USERNAME"

# As we will use a temporary location to pass files to the downloader, set that up
dir_tmp="/tmp/debear/seed/$git_repo-$$"
mkdir -p $dir_tmp

#
# Get the test data files?
#
# Pass 1: Check via the MD5 cache if we actually need to download any files?
echo -n "Download Test Data: "
dl_flag="$dir_data/.cache"
if [ -e $dl_flag ]
then
    sed -r -e 's/\t/=/g' -e 's/\//%2F/g' $dl_flag | sed -e ':a;N;$!ba;s/\n/\&/g' >$dir_tmp/current
fi
dl_HEAD=$($dir_scripts/download.sh database "$git_repo" "$git_branch" --header "X-HEAD:true" --data @$dir_tmp/current 2>&1)

# Are we pruning any files?
if [ ! -z "$dl_HEAD" ]
then
    prune=$(echo "$dl_HEAD" | sed 's/\&/\n/g' | grep -F '_pruned_' | sed 's/=_pruned_//')
    for tbl in ${prune[@]}
    do
        rm -rf $dir_data/$tbl
        grep -Fv "$tbl" $dl_flag >$dl_flag.pruned
        mv $dl_flag.pruned $dl_flag
        dl_HEAD=$(echo "$dl_HEAD" | sed "s@$tbl=_pruned_@@")
    done
    dl_HEAD=$(echo "$dl_HEAD" | sed -r 's/\&{2,}/\&/g' | sed 's/^\&//g' | sed 's/\&$//g')
fi

# Pass 2: If it returns a list, we need to download these files
if [ ! -z "$dl_HEAD" ] || [ ! -e $dl_flag ]
then
    echo "$dl_HEAD" >$dir_tmp/download
    $dir_scripts/download.sh database "$git_repo" "$git_branch" --data @$dir_tmp/download --output $dir_data/database.tar.gz
    if [ ! -e "$dir_data/database.tar.gz" ]
    then
        echo "[ Failed ]"
        exit 1
    fi
    tar -xzf $dir_data/database.tar.gz -C $dir_data
    rm -rf $dir_data/database.tar.gz
    echo "[ Done ]"
else
    echo "[ Skipped ]"
fi
echo

#
# Create the databases, when setting up as the skeleton
#
if [ "x$git_repo" = 'xskeleton' ]
then
    $dir_root/setup
    echo
fi

#
# Run the schema builder
#
$dir_root/schema $git_repo
echo

#
# Now go through our databases and import the files
#
display_title "Seeding Test Database(s)"
cd "$dir_data"
for db in $(ls -d *)
do
    echo
    test_db="${DB_PREFIX}$db"
    display_section "$db (as '$test_db'):"
    cd "$dir_data/$db"
    for tbl in $(ls -d *.sql.gz)
    do
        tbl_name=$(echo "$tbl" | sed 's/\.sql\.gz$//')
        display_info "  $tbl_name"
        gzip -dc $tbl | mysql $mysql_args $test_db
    done
done
