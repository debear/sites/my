#!/bin/bash
# Download a file from the CI Data server

# Args passed in
file_type="$1"; shift
git_repo="$1"; shift
git_branch="$1"; shift

# Determine the git repo info (if not passed in)
dir_scripts=$(dirname $0)
if [ -z "$git_repo" ] || [ "x$git_repo" = 'x-' ]
then
    git_repo=$($dir_scripts/git.sh repo)
fi
if [ -z "$git_branch" ] || [ "x$git_branch" = 'x-' ]
then
    git_branch=$($dir_scripts/git.sh branch)
fi
# Safe-some chars in the branch
git_branch=$(echo "$git_branch" | sed -r 's/\//./')

# Secret management for environments that do not have CI_DATA_* vars available
if [ -e /var/www/debear/etc/ci/env ]
then
    source /var/www/debear/etc/ci/env
fi
mkdir -p /tmp/debear
echo "$CI_DATA_TRUST" >/tmp/debear/trust.crt

# Now make the request
curl --silent $(echo $@) --digest --user "$CI_DATA_USER:$CI_DATA_PASSWORD" --cacert /tmp/debear/trust.crt https://$CI_DATA_DOMAIN/$git_repo/$git_branch/$file_type
