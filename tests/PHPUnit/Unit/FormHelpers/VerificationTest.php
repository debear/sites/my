<?php

namespace Tests\PHPUnit\Unit\FormHelpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\My\Traits\UnitTest\IncludeRefreshDatabase;
use DeBear\Http\Forms\My\Verification;
use DeBear\Models\Skeleton\User;
use DeBear\Helpers\Policies;

class VerificationTest extends UnitTestCase
{
    use IncludeRefreshDatabase;

    /**
     * A test of processing edge-case where someone else is logged in.
     * @return void
     */
    public function testProcessingOtherUser(): void
    {
        // Log in as someone else.
        User::doLogin(User::find(10)); // 10 == test_user.
        $this->assertTrue(User::object()->isLoggedIn());
        $this->assertEquals('test_user', User::object()->user_id);
        $this->assertTrue(User::object()->isVerified());
        // Then attempt processing the unverified user.
        Verification::process(User::find(20)); // 20 == test_unverified.
        $this->assertTrue(User::object()->isLoggedIn());
        $this->assertEquals('test_unverified', User::object()->user_id);
        $this->assertTrue(User::object()->isVerified());
    }

    /**
     * A test of processing edge-case where the user is already logged in.
     * @return void
     */
    public function testProcessingPolicyRecalc(): void
    {
        // Log in as someone else.
        User::doLogin(User::find(20)); // 20 == test_unverified.
        $this->assertTrue(User::object()->isLoggedIn());
        $this->assertEquals('test_unverified', User::object()->user_id);
        $this->assertTrue(User::object()->isUnverified());
        $this->assertFalse(Policies::match('user:verified'));
        $this->assertTrue(Policies::match('user:unverified'));
        // Then attempt processing as this user.
        Verification::process(User::object());
        $this->assertTrue(User::object()->isLoggedIn());
        $this->assertEquals('test_unverified', User::object()->user_id);
        $this->assertTrue(User::object()->isVerified());
        $this->assertTrue(Policies::match('user:verified'));
        $this->assertFalse(Policies::match('user:unverified'));
    }
}
