<?php

namespace Tests\PHPUnit\Unit\FormHelpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Http\Forms\My\Account;
use DeBear\Models\Skeleton\CommsEmail;
use DeBear\Models\Skeleton\User;

class AccountTest extends UnitTestCase
{
    /**
     * A test of User ID validation.
     * @return void
     */
    public function testValidateUser(): void
    {
        // Prepare the user object.
        User::setup();
        FrameworkConfig::set(['debear.security.banned_ids' => FrameworkConfig::get('debear.my.security.banned_ids')]);
        // Blacklisted User ID.
        $this->assertEquals('This username is not allowed', Account::validateUserID('support'));
        // Existing User ID.
        $this->assertEquals('This username is not allowed', Account::validateUserID('test_user'));
        // All okay.
        $this->assertFalse(Account::validateUserID('something-unused'));
    }

    /**
     * A test of password validation.
     * @return void
     */
    public function testValidatePassword(): void
    {
        // Current: Mismatch.
        $this->assertEquals('Please enter your current password', Account::validatePasswordFields(
            false,
            [
                'pass_new1' => 'new',
            ],
            [
                'mode' => 'curr',
                'curr' => 'pass_curr',
                'new' => 'pass_new',
            ]
        ));
        // Current: Okay.
        $this->assertFalse(Account::validatePasswordFields(
            false,
            [
                'pass_curr' => 'old',
                'pass_new1' => 'new',
                'pass_new2' => 'new',
            ],
            [
                'mode' => 'curr',
                'curr' => 'pass_curr',
                'new' => 'pass_new',
            ]
        ));
        // New: Mismatch.
        $this->assertEquals('Please enter a new password', Account::validatePasswordFields(
            false,
            [
                'pass_curr' => 'curr',
                'pass_new1' => 'new',
            ],
            [
                'mode' => 'new',
                'curr' => 'pass_curr',
                'new' => 'pass_new',
            ]
        ));
        // New: Okay.
        $this->assertFalse(Account::validatePasswordFields(
            false,
            [
                'pass_curr' => 'old',
                'pass_new1' => 'new',
                'pass_new2' => 'new',
            ],
            [
                'mode' => 'new',
                'curr' => 'pass_curr',
                'new' => 'pass_new',
            ]
        ));
    }

    /**
     * A test of confirmation email sends.
     * @return void
     */
    public function testConfirmationEmails(): void
    {
        $where = [
            'app' => FrameworkConfig::get('debear.my.email.app'),
            'email_reason' => 'account_update',
        ];
        // Count before.
        $num_before = CommsEmail::where($where)->count();
        // Process with no changes.
        Account::confirmationEmail([
            'raw' => [
                'changes' => [
                    'fields' => [],
                ],
            ],
        ]);
        // Count after.
        $num_after = CommsEmail::where($where)->count();
        // Ensure the same!
        $this->assertEquals($num_after, $num_before);
    }
}
