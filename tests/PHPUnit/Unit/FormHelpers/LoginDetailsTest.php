<?php

namespace Tests\PHPUnit\Unit\FormHelpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\My\Traits\UnitTest\IncludeRefreshDatabase;
use DeBear\Http\Forms\My\LoginDetails;
use DeBear\Models\Skeleton\User;
use DeBear\Models\Skeleton\UserActivityLog;
use DeBear\Models\Skeleton\CommsEmail;

class LoginDetailsTest extends UnitTestCase
{
    use IncludeRefreshDatabase;

    /**
     * A test of passing unknown user details to the request processor.
     * @return void
     */
    public function testProcessingUnknown(): void
    {
        $where = [
            'type' => 3,
        ];
        // Count of suspicious activity logs before.
        $num_before = UserActivityLog::where($where)->count();
        // Attempt processing.
        LoginDetails::processRequest([
            'raw' => [
                'proc' => [
                    'email' => 'unknown@example.com',
                ],
            ],
        ]);
        // Count of suspicious activity logs after.
        $num_after = UserActivityLog::where($where)->count();

        // Ensure it's been bumped by one.
        $this->assertGreaterThan($num_before, $num_after);
        $this->assertEquals($num_after, $num_before + 1);
    }

    /**
     * A test of the email output when an account is linked to a single email address.
     * @return void
     */
    public function testProcessingSingleEmail(): void
    {
        // Update the test accounts to singularise the email address.
        $user = User::find(10); // 10 == test_user.
        $user->email = 'test.unique@debear.uk';
        $user->save();
        // Run the processing.
        LoginDetails::processRequest([
            'raw' => [
                'proc' => [
                    'email' => $user->email,
                ],
            ],
        ]);
        // Get the email out and check the wording.
        $email = CommsEmail::where([
            'app' => 'admin',
            'email_reason' => 'account_reset_request',
        ])->orderBy('email_queued', 'desc')->limit(1)->first();
        $this->assertStringContainsString(
            "&lt;p&gt;The password for your account &lt;code&gt;{$user->user_id}&lt;/code&gt;",
            $email->email_body
        );
    }
}
