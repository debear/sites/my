<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\CommsEmail;
use DeBear\Models\Skeleton\CommsEmailLink;
use DeBear\Models\Skeleton\User;
use DeBear\Helpers\Strings;

class LoginResetTest extends FeatureTestCase
{
    /**
     * A test that we are redirect to the account page when already logged in
     * @return void
     */
    public function testRedirect(): void
    {
        // Log in.
        $post = [
            'username' => 'test_unverified',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        // Then process.
        $response = $this->get('/login-details');
        $response->assertRedirect('/account', 307);
    }

    /**
     * A test of password resetting
     * @return void
     */
    public function testProcess(): void
    {
        // First render.
        $response = $this->get('/login-details');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Login Details</h1>');
        $response->assertSee('<input class="textbox" type="text" id="email" name="email" value=""'
            . ' autocomplete="email" required tabindex="1" data-tabindex="1" data-email="true">');

        // And push changes - at first, with a security code (that should fail).
        $post = [
            'form_ref' => 'a091ff',
            'email' => 'test@debear.uk',
            'sec_code' => '123456',
        ];
        $response = $this->post('/login-details', $post);
        $response->assertRedirect('/login-details', 303);
        // Re-request with updated form.
        $response = $this->get('/login-details');
        $response->assertStatus(200);
        $response->assertSee('<h1>
    DeBear.uk Login Details
</h1>');
        $response->assertSee('Login Details request processed!');

        // Get the link for Phase II.
        $email = CommsEmail::where('email_reason', '=', 'account_reset_request')->first();
        preg_match(
            '@&lt;li&gt;&lt;code&gt;test_unverified&lt;/code&gt;: &lt;a href=&quot;(.*?)&quot;&gt;Reset '
            . 'Link&lt;/a&gt;&lt;/li&gt;@',
            $email->email_body,
            $link
        );
        $pwd_updater = preg_replace('@https?://[^/]+/@', '/', $link[1]);

        $link_code = explode('-', $pwd_updater);
        $raw_link = CommsEmailLink::where('link_code', '=', $link_code[1])->first();

        // Now check the links trigger correctly.
        $response = $this->get($pwd_updater);
        $response->assertRedirect($raw_link->link_url, 303);

        $response = $this->get($raw_link->link_url);
        $response->assertRedirect('/account/reset', 303);

        // Then the resulting form allows us to update passwords.
        $response = $this->get('/account/reset');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Password Reset</h1>');
        $response->assertSee('<input class="textbox" type="password" id="password1" name="password1" value=""'
            . ' autocomplete="new-password" required minlength="8" tabindex="1" data-tabindex="1"'
            . ' data-status-field="password">');

        // And push changes - at first, with mis-matching (that should fail).
        $post = [
            'form_ref' => '8eb234',
            'password1' => 'logindetailstest',
            'password2' => 'somethingdifferent',
        ];
        $response = $this->post('/account/reset', $post);
        $response->assertRedirect('/account/reset', 303);
        // Re-request with updated form.
        $response = $this->get('/account/reset');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Password Reset</h1>');
        // Catch security code fail.
        $response->assertSee('<div class="box error icon_error ">');
        $response->assertSee('<li class="label error" id="password_label">');

        // And then without these issues (which should succeed).
        $post['password2'] = $post['password1'];
        $response = $this->post('/account/reset', $post);
        $response->assertRedirect('/account/reset', 303);

        // Then log out, and back-in with these new details.
        $response = $this->post('/logout');
        $response->assertStatus(200);

        $post = [
            'username' => 'test_unverified',
            'password' => 'logindetailstest',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        // Revert for future tests...
        $user = User::where('user_id', '=', 'test_unverified')->first();
        $user->update([
            'password' => Strings::oneWayHash('testlogindetails', 'test_unverified'),
        ])->save();
    }

    /**
     * A test of password resetting
     * @return void
     */
    public function testProcessOtherSubdomain(): void
    {
        // Forgotten Details.
        $response = $this->get('/login-details', [
            'X-DeBear-Host' => 'my-faux.' . FrameworkConfig::get('debear.url.base'),
        ]);
        $response->assertRedirect('https://my.' . FrameworkConfig::get('debear.url.base') . '/login-details', 303);

        // Registration.
        $response = $this->get('/register', [
            'X-DeBear-Host' => 'my-faux.' . FrameworkConfig::get('debear.url.base'),
        ]);
        $response->assertRedirect('https://my.' . FrameworkConfig::get('debear.url.base') . '/register', 303);
    }

    /**
     * A test of password reset for an unknown user
     * @return void
     */
    public function testResetUnknownUser(): void
    {
        // Generate a timelock based on a user who doesn't exist.
        $response = $this->get('/account/reset-' . Strings::generateUserTimeLock('no-matching-user'));
        $response->assertStatus(400);
    }

    /**
     * A test of password reset when somebody else is already logged in
     * @return void
     */
    public function testResetOtherUser(): void
    {
        // Log in as User A.
        $post = [
            'username' => 'test_unverified',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        // Then perform a password reset for User B.
        $response = $this->get('/account/reset-' . Strings::generateUserTimeLock('test_legacy'));
        $response->assertRedirect('/account/reset', 303);
    }
}
