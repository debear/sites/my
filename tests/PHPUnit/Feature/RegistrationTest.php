<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class RegistrationTest extends FeatureTestCase
{
    /**
     * A test that we are redirect to the account page when already logged in
     * @return void
     */
    public function testRedirect(): void
    {
        // Log in.
        $post = [
            'username' => 'test_unverified',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        // Then process.
        $response = $this->get('/register');
        $response->assertRedirect('/account', 307);
    }

    /**
     * A test of registration processing
     * @return void
     */
    public function testProcess(): void
    {
        // First render.
        $response = $this->get('/register');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk User Registration</h1>');
        $response->assertSee('<input class="textbox" type="text" id="user_id" name="user_id" value=""'
            . ' autocomplete="username" required minlength="5" maxlength="20"');

        // And push changes - at first, with a security code (that should fail).
        $post = [
            'form_ref' => 'c81d18',
            'user_id' => 'test_register',
            'email' => 'test@debear.uk',
            'forename' => 'Registered',
            'surname' => 'User',
            'display_name' => 'Registered User',
            'dob' => '1970-01-01',
            'timezone' => 'Europe/London',
            'terms' => 'on',
            'sec_code' => '123456',
        ];
        $response = $this->post('/register', $post);
        $response->assertRedirect('/register', 303);
        // Re-request with updated form.
        $response = $this->get('/register');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk User Registration</h1>');
        $response->assertSee('<input class="textbox" type="text" id="display_name" name="display_name"'
            . ' value="Registered User" autocomplete="nickname" required maxlength="20"');
        // Catch security code fail.
        $response->assertSee('<div class="box error icon_error ">');
        $response->assertSee('<li class="label error" id="sec_code_label">');

        // Then with mis-matching passwords (which should fail).
        unset($post['sec_code']);
        $post['password1'] = 'logindetailstest';
        $post['password2'] = 'somethingdifferent';
        $response = $this->post('/register', $post);
        $response->assertRedirect('/register', 303);
        // Re-request with updated form.
        $response = $this->get('/register');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk User Registration</h1>');
        // Catch current password fail.
        $response->assertSee('<div class="box error icon_error ">');
        $response->assertSee('<li class="label error" id="password_label">');

        // And then without these issues (which should succeed).
        $post['password2'] = $post['password1'];
        $response = $this->post('/register', $post);
        $response->assertRedirect('/register', 303);

        // Then log out, and back-in with these new details.
        $response = $this->post('/logout');
        $response->assertStatus(200);

        $post = [
            'username' => 'test_register',
            'password' => 'logindetailstest',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);
    }
}
