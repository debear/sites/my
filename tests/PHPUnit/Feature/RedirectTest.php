<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class RedirectTest extends FeatureTestCase
{
    /**
     * A test of / when not logged in
     * @return void
     */
    public function testRedirectGuest(): void
    {
        // Ensure logged out first.
        $this->post('/logout');

        // Then process.
        $response = $this->get('/');
        $response->assertRedirect('/register', 307);
    }

    /**
     * A test of / when logged in
     * @return void
     */
    public function testRedirectLoggedIn(): void
    {
        // Log in.
        $post = [
            'username' => 'test_unverified',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        // Then process.
        $response = $this->get('/');
        $response->assertRedirect('/account', 307);
    }
}
