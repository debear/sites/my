<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Helpers\Strings;

class ValidateVerifyTest extends FeatureTestCase
{
    /**
     * A test of user validation
     * @return void
     */
    public function testValidate(): void
    {
        // Missing its argument.
        $response = $this->get('/validate/user_id');
        $response->assertStatus(200);
        $response->assertJSON([
            'valid' => false,
            'message' => 'Missing argument to test',
        ]);
        // An empty argument.
        $response = $this->get('/validate/user_id?user_id');
        $response->assertStatus(200);
        $response->assertJSON([
            'valid' => false,
            'message' => 'Missing argument to test',
        ]);
        // Unknown user.
        $response = $this->get('/validate/user_id?user_id=nomatchinguser');
        $response->assertStatus(200);
        $response->assertJSON([
            'valid' => true,
            'message' => false,
        ]);
        // Known user.
        $response = $this->get('/validate/user_id?user_id=test_user');
        $response->assertStatus(200);
        $response->assertJSON([
            'valid' => false,
            'message' => 'This username is not allowed',
        ]);
    }

    /**
     * A test of unknown user verification
     * @return void
     */
    public function testVerifyUnknown(): void
    {
        $response = $this->get('/account/verify-' . Strings::generateUserTimeLock('no-matching-user'));
        $response->assertStatus(400);
    }

    /**
     * A test of a previously verified user
     * @return void
     */
    public function testVerifyPrevious(): void
    {
        $response = $this->get('/account/verify-' . Strings::generateUserTimeLock('test_user'));
        $response->assertStatus(400);
        $response->assertSee('This email address has been previously verified');
    }

    /**
     * A test of the verification complete exceptions
     * @return void
     */
    public function testVerifyCompleteExceptions(): void
    {
        // Not a logged in user.
        $response = $this->get('/account/verified');
        $response->assertRedirect('/register');

        // An unverified user.
        // - Login.
        $post = [
            'username' => 'test_unverified',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);
        // - Process.
        $response = $this->get('/account/verified');
        $response->assertStatus(403);
    }

    /**
     * A test of the verification process
     * @return void
     */
    public function testVerifyProcess(): void
    {
        // Initial stage.
        $response = $this->get('/account/verify-' . Strings::generateUserTimeLock('test_unverified'));
        $response->assertRedirect('/account/verified');
        // Then the verification complete.
        $response = $this->get('/account/verified');
        $response->assertStatus(200);
        $response->assertSee('Your email address has been verified');
    }

    /**
     * A test of resending the verification email
     * @return void
     */
    public function testVerifyResend(): void
    {
        // Not logged in.
        $response = $this->patch('/account/resend-verify');
        $response->assertStatus(403);
        $response->assertJSON([
            'success' => false,
        ]);

        // Previously verified user.
        // - Login.
        $post = [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);
        // - Process.
        $response = $this->patch('/account/resend-verify');
        $response->assertStatus(409);
        $response->assertJSON([
            'success' => false,
        ]);

        // Processing.
        // - Logout from previous test.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        // - Login.
        $post = [
            'username' => 'test_unverified',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);
        // - Process.
        $response = $this->patch('/account/resend-verify');
        $response->assertStatus(200);
        $response->assertJSON([
            'success' => true,
        ]);
    }
}
