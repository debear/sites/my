<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class AccountTest extends FeatureTestCase
{
    /**
     * A test that we are redirect to the register page when not logged in
     * @return void
     */
    public function testRedirect(): void
    {
        // Ensure logged out first.
        $this->post('/logout');

        // Then process.
        $response = $this->get('/account');
        $response->assertRedirect('/register', 307);
    }

    /**
     * A test of account processing
     * @return void
     */
    public function testProcess(): void
    {
        // Log in.
        $post = [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        // Then render.
        $response = $this->get('/account');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Account Details</h1>');
        $response->assertSee('<input class="textbox" type="text" id="user_id" name="user_id" value="test_user"'
            . ' autocomplete="username" required minlength="5" maxlength="20"');

        // And push changes - at first, with a security code (that should fail).
        $post = [
            'form_ref' => '88ecba',
            'user_id' => 'test_user',
            'email' => 'test@debear.uk',
            'forename' => 'Updated',
            'surname' => 'User',
            'display_name' => 'Updated User',
            'dob' => '1970-01-01',
            'timezone' => 'Europe/London',
            'sec_code' => '123456',
        ];
        $response = $this->post('/account', $post);
        $response->assertRedirect('/account', 303);
        // Re-request with updated form.
        $response = $this->get('/account');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Account Details</h1>');
        $response->assertSee('<input class="textbox" type="text" id="display_name" name="display_name"'
            . ' value="Updated User" autocomplete="nickname" required maxlength="20"');
        // Catch security code fail.
        $response->assertSee('<div class="box error icon_error ">');
        $response->assertSee('<li class="label error" id="sec_code_label">');

        // Then with mis-matching passwords (which should fail).
        unset($post['sec_code']);
        $post['password_curr'] = 'somethingwrong';
        $post['password_new1'] = $post['password_new2'] = 'logindetailstest';
        $response = $this->post('/account', $post);
        $response->assertRedirect('/account', 303);
        // Re-request with updated form.
        $response = $this->get('/account');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Account Details</h1>');
        // Catch current password fail.
        $response->assertSee('<div class="box error icon_error ">');
        $response->assertSee('<li class="label error" id="password_curr_label">');

        // Then with wrong current password (which should fail).
        $post['password_curr'] = 'testlogindetails';
        $post['password_new1'] .= 'l';
        $response = $this->post('/account', $post);
        $response->assertRedirect('/account', 303);
        // Re-request with updated form.
        $response = $this->get('/account');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Account Details</h1>');
        // Catch new password fail.
        $response->assertSee('<div class="box error icon_error ">');
        $response->assertSee('<li class="label error" id="password_label">');

        // And then without these issues (which should succeed).
        $post['password_new1'] = $post['password_new2'];
        $response = $this->post('/account', $post);
        $response->assertRedirect('/account', 303);

        // Then log out, and back-in with these new details.
        $response = $this->post('/logout');
        $response->assertStatus(200);

        $post = [
            'username' => 'test_user',
            'password' => 'logindetailstest',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);
    }
}
