#!/bin/bash
# Clone the skeleton in the parent directory
echo -e "\e[1mOn CI server, so cloning _debear to parent directory:\e[0m"

# Some config
dir_tests=$(realpath $(dirname $0)/../..)
dir_root=$(realpath $dir_tests/..)
dir_scripts=$(realpath $dir_tests/_scripts)

# Clone and load our previous vendor folder
$dir_scripts/git-clone.sh skeleton _debear

# Setup our environment in advance of the tests
cd $dir_root/../_debear
tests/setup
