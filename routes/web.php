<?php

/**
 * Routes for the My sub-domain
 */

Route::get(
    '/',
    function () {
        // Homepage depends on the role.
        if (!\DeBear\Models\Skeleton\User::object()->isLoggedIn()) {
            // Logged out user should register.
            return redirect('/register', 307);
        } else {
            // Logged in users can access their account details.
            return redirect('/account', 307);
        }
    }
);

// User ID validation.
Route::get('/validate/user_id', 'My\Validation@userId');

// Email Verification.
Route::get('/account/verify-{code}', 'My\Verification@index')
            ->where('code', '[A-Za-z0-9]+');
Route::get('/account/verified', 'My\Verification@complete');
Route::patch('/account/resend-verify', 'My\Verification@resend');

// Password reset.
Route::get('/account/reset-{code}', 'My\PasswordReset@index');
Route::match(['get', 'post'], '/account/reset', 'My\PasswordReset@reset');

// My Account.
Route::match(['get', 'post'], '/account', 'My\Account@index');
