@extends('skeleton.layouts.html')

@section('content')

<h1>{!! $site_name !!} Email Previously Verified</h1>
<div class="box error icon_delete">This email address was previously verified</div>
<p>
    Oops, this email address has already been verified so no action has been taken. Don&#39;t worry though, this won&#39;t change anything about your {!! $site_name !!} user account!
</p>

@endsection
