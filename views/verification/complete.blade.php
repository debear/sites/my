@extends('skeleton.layouts.html')

@section('content')

<h1>{!! $site_name !!} Email Verified</h1>
<div class="box success icon_valid">Your email address has been verified!</div>
<p>
    Congratulations! This completes the registration process and your {!! $site_name !!} user account has now been created and is fully active.
</p>

<h3>What if I forget my details to sign in?</h3>
<p>
    Worry not! If you click the "<em>I've forgotten my details to sign in!</em>" link on the Sign In panel, simply follow the on-screen instructions to recover your User ID and/or Password.
</p>

@endsection
