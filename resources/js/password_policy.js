get('dom:load').push(() => {
    Events.attach(document, 'click', (e) => {
        if (e.target.classList.contains('view-password-policy')) {
            // Display the modal
            DOM.child('.password-policy').classList.remove('hidden');
        } else if (e.target.classList.contains('close-password-policy')) {
            // Hide the modal
            DOM.child('.password-policy').classList.add('hidden');
        }
    });
});
