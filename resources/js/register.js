// Show/Hide the Help
toggleHelp = (ele) => {
    var help = ele.closest('.head').querySelector('.help');
    help.classList.toggle('hidden');
    ele.innerHTML = (help.classList.contains('hidden') ? 'Show' : 'Hide') + ' Help';
}

// Show the Terms of Use
showTerms = () => {
    window.open('/terms-of-use?interactive=1', '_terms', 'width=500px,height=500px,directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no');
}

defaultDisplayName = () => {
    return (DOM.child('#forename').value + ' ' + DOM.child('#surname').value.substring(0, 1)).trim();
}

/**
 * Custom validation rules
 */
// User ID must be unique
Forms.validateCustomUserId = (valid) => {
    // Skip if not valid up to this point
    var message = '';
    var userId = DOM.child('#user_id').value.toString().trim();
    if (valid && userId !== '') {
        get('forms:ajax').request({
            'url': `/validate/user_id?user_id=${userId}`,
            'success': (retValue) => {
                // Only really need to process when it's not valid
                if (!Input.isObject(retValue) || Input.isTrue(retValue.valid)) {
                    return;
                }
                // Process the error
                ele = DOM.child('#user_id');
                var validation = get('forms:validation')[ele.closest('form').id][ele.id]
                validation.valid = false;
                validation.errors.push('custom:user_id');
                validation.message = validation.message || retValue.message;
                Forms.validationDisplay(ele, ele.value, validation);
            }
        });
    }
    return {
        'valid': valid,
        'message': message
    };
}

// Password1 == Password2
Forms.validateCustomPassword1 = Forms.validateCustomPassword2 = (valid) => {
    // Get the two passwords
    var pwd1 = DOM.child('#password1').value.toString().trim();
    var pwd2 = DOM.child('#password2').value.toString().trim();
    // Skip if either is blank (we're not ready yet)
    if (pwd1 === '' || pwd2 === '') {
        return {
            'valid': valid,
            'skip': true,
            'message': ''
        };
    }

    var equal = (pwd1 === pwd2);
    return {
        'valid': equal,
        'message': equal ? '' : 'Please ensure the two passwords match'
    };
}

/**
 * Standard onload event for this form
 */
get('dom:load').push(() => {
    // Toggles
    Events.attach(DOM.child('.help-toggle'), 'click', (e) => { toggleHelp(e.target); });
    Events.attach(DOM.child('.terms-show'), 'click', () => { showTerms(); });

    // Default display name (Firstname SurnameInitial)
    set('forms:register:custom-disp-name', false);
    Events.attach(DOM.child('#display_name'), 'keyup', (e) => {
        set('forms:register:custom-disp-name', e.target.value != defaultDisplayName());
    });

    ['forename', 'surname'].forEach((ele) => {
        Events.attach(DOM.child(`#${ele}`), 'keyup', () => {
            // Only update if the display name hasn't been customised
            if (!get('forms:register:custom-disp-name')) {
                DOM.child('#display_name').value = defaultDisplayName();
            }
        });
    });

    // Unset a faux required field
    DOM.child('#sec_code').required = false;

    // Create our Ajax object for processing
    set('forms:ajax', new Ajax());

    // Password1 == Password2 validation tweaks
    Events.attach(document, 'debear:form:validation:password1', (e) => {
        if (e.detail.valid && DOM.child('#password2').classList.contains('error')) {
            DOM.child('#password2').classList.remove('error');
        }
    });
    Events.attach(document, 'debear:form:validation:password2', (e) => {
        if (e.detail.valid && DOM.child('#password1').classList.contains('error')) {
            DOM.child('#password1').classList.remove('error');
        }
    });
});
