/**
 * Custom validation rules
 */
// Password1 == Password2
Forms.validateCustomPassword1 = Forms.validateCustomPassword2 = (valid) => {
    // Get the two passwords
    var pwd1 = DOM.child('#password1').value.toString().trim();
    var pwd2 = DOM.child('#password2').value.toString().trim();
    // Skip if either is blank (we're not ready yet)
    if (pwd1 === '' || pwd2 === '') {
        return {
            'valid': valid,
            'skip': true,
            'message': ''
        };
    }

    var equal = (pwd1 === pwd2);
    return {
        'valid': equal,
        'message': equal ? '' : 'Please ensure the two passwords match'
    };
}

/**
 * Standard onload event for this form
 */
get('dom:load').push(() => {
    // Password1 == Password2 validation tweaks
    Events.attach(document, 'debear:form:validation:password1', (e) => {
        if (e.detail.valid && DOM.child('#password2').classList.contains('error')) {
            DOM.child('#password2').classList.remove('error');
        }
    });
    Events.attach(document, 'debear:form:validation:password2', (e) => {
        if (e.detail.valid && DOM.child('#password1').classList.contains('error')) {
            DOM.child('#password1').classList.remove('error');
        }
    });
});
