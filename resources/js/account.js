/**
 * Custom validation rules
 */
// User ID must be unique
Forms.validateCustomUserId = (valid) => {
    // Skip if not valid up to this point
    var message = '';
    var userId = DOM.child('#user_id').value.toString().trim();
    if (valid && userId !== '') {
        get('forms:ajax').request({
            'url': `/validate/user_id?user_id=${userId}`,
            'success': (retValue) => {
                // Only really need to process when it's not valid
                if (!Input.isObject(retValue) || Input.isTrue(retValue.valid)) {
                    return;
                }
                // Process the error
                ele = DOM.child('#user_id');
                var validation = get('forms:validation')[ele.closest('form').id][ele.id]
                validation.valid = false;
                validation.errors.push('custom:user_id');
                validation.message = validation.message || retValue.message;
                Forms.validationDisplay(ele, ele.value, validation);
            }
        });
    }
    return {
        'valid': valid,
        'message': message
    };
}

// Current Password if New Password set
Forms.validateCustomPasswordCurr = () => {
    // Get the passwords
    var pwdC = DOM.child('#password_curr').value.toString().trim();
    var pwd1 = DOM.child('#password_new1').value.toString().trim();
    var pwd2 = DOM.child('#password_new2').value.toString().trim();

    // If we're missing the current password, but have at least one new password, fail
    if (pwdC === '' && (pwd1 !== '' || pwd2 !== '')) {
        return {
            'valid': false,
            'force': true,
            'message': 'Please enter your current password'
        };
    }

    return {
        'valid': true,
        'message': ''
    };
}

// Password1 == Password2
Forms.validateCustomPasswordNew1 = Forms.validateCustomPasswordNew2 = (valid) => {
    // Get the two passwords
    var pwdC = DOM.child('#password_curr').value.toString().trim();
    var pwd1 = DOM.child('#password_new1').value.toString().trim();
    var pwd2 = DOM.child('#password_new2').value.toString().trim();

    if ((pwd1 !== '' && pwd2 === '') || (pwd1 === '' && pwd2 !== '')) {
        // If one is set, but not the other then we need to flag this
        return {
            'valid': false,
            'message': 'Please enter a new password'
        };
    } else if (pwd1 === '' && pwd2 === '') {
        // Skip if both are blank (we're not ready yet)
        // If we have no current password, clear that error
        Forms.clearFailedField(DOM.child('#password_curr'));
        // Return our status
        return {
            'valid': valid,
            'skip': true,
            'message': ''
        };
    }

    // Test the equality of our new password
    var equal = (pwd1 === pwd2);

    // If both are set and equal and we have no current password, fail with error
    if (equal && pwdC === '') {
        Forms.failField(DOM.child('#password_curr'), 'required', 'Please enter your current password');
    }

    // Return our status
    return {
        'valid': equal,
        'message': equal ? '' : 'Please ensure the two passwords match'
    };
}

/**
 * Standard onload event for this form
 */
get('dom:load').push(() => {
    // Unset a faux required field
    DOM.child('#sec_code').required = false;

    // Create our Ajax object for processing
    set('forms:ajax', new Ajax());

    // Password1 == Password2 validation tweaks
    Events.attach(document, 'debear:form:validation:password_new1', (e) => {
        if (e.detail.valid && DOM.child('#password_new2').classList.contains('error')) {
            DOM.child('#password_new2').classList.remove('error');
        }
    });
    Events.attach(document, 'debear:form:validation:password_new2', (e) => {
        if (e.detail.valid && DOM.child('#password_new1').classList.contains('error')) {
            DOM.child('#password_new1').classList.remove('error');
        }
    });
});
