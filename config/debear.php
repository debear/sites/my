<?php

/**
 * Config specifically for the my sub-domain
 */

return [
    /*
     * Site name details
     */
    'names' => [
        'section' => 'My DeBear.uk',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'enabled' => false,
            'descrip' => 'User Account Management for DeBear.uk',
        ],
        'sitemap' => [
            'enabled' => false,
        ],
    ],

    /*
     * Site revision info
     */
    'version' => [
        'breakdown' => [
            'major' => 2,
            'minor' => 0,
        ],
    ],

    /*
     * Google Analytics
     */
    'analytics' => [
        'urchins' => [
            'tracking' => [ 'UA-48789782-8' ],
        ],
    ],

    /*
     * Banned User IDs
     */
    'security' => [
        'banned_ids' => [
            'support', 'supp0rt', '5upport', '5upp0rt',
            'admin', '4dmin', 'adm1n', '4dm1n',
            'superadmin', 'super4dmin', 'superadm1n', 'super4dm1n',
            'sup3radmin', 'sup3r4dmin', 'sup3radm1n', 'sup3r4dm1n',
            '5uperadmin', '5uper4dmin', '5uperadm1n', '5uper4dm1n',
            '5up3radmin', '5up3r4dmin', '5up3radm1n', '5up3r4dm1n',
            'superuser', 'sup3ruser', 'superus3r', 'sup3rus3r',
            'superu5er', 'sup3rus5r', 'superu53r', 'sup3ru53r',
            '5uperuser', '5up3ruser', '5uperus3r', '5up3rus3r',
            '5uperu5er', '5up3rus5r', '5uperu53r', '5up3ru53r',
        ],
    ],

    /*
     * Page content values
     */
    'content' => [
        'header' => [
            'url' => false, // Never have a homepage link.
        ],
    ],

    /*
     * Verification details
     */
    'verification' => [
        'timeout' => 72, // Hours.
    ],

    /*
     * Login details
     */
    'login_details' => [
        'timeout' => 24, // Hours.
    ],
];
