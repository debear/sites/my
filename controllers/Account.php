<?php

namespace DeBear\Http\Controllers\My;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Http\Modules\Forms;

class Account extends Controller
{
    /**
     * Form for users to update their details
     * @return Response|RedirectResponse The relevant response, which could be a redirect if not a valid request
     */
    public function index(): Response|RedirectResponse
    {
        // Process and render.
        $obj = new Forms('my/account');
        return $obj->run();
    }
}
