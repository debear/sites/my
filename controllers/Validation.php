<?php

namespace DeBear\Http\Controllers\My;

use Illuminate\Support\Facades\Request;
use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Http\Forms\My\Account;

class Validation extends Controller
{
    /**
     * Test for the uniqueness of a requested user_id
     * @return JsonResponse A response object with the status of our user_id uniqueness test
     */
    public function userId(): JsonResponse
    {
        $user_id = Request::get('user_id');

        // Perform the validation?
        if (isset($user_id) && $user_id) {
            $check = Account::validateUserID($user_id);
        } else {
            $check = 'Missing argument to test';
        }

        // Return as JSON object to the browser.
        return response()->json(
            [
                'valid' => is_bool($check) && !$check,
                'message' => $check,
            ]
        );
    }
}
