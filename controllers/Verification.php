<?php

namespace DeBear\Http\Controllers\My;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Http\Forms\My\Verification as VerificationHelper;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Strings;
use DeBear\Models\Skeleton\User;

class Verification extends Controller
{
    /**
     * Process the email verification request
     * @param string $code The verification code passed in by the User to identify the attempt.
     * @return Response|RedirectResponse A redirect if a valid (and processed) request, or an error response
     */
    public function index(string $code): Response|RedirectResponse
    {
        // Validate the code and user.
        $user_id = Strings::validateUserTimeLock($code, FrameworkConfig::get('debear.verification.timeout'));
        if (is_string($user_id)) {
            $user = User::where([['user_id', '=', $user_id]])->first();
        }

        if (!isset($user)) {
            return HTTP::sendBadRequest();
        } elseif (!$user->isUnverified()) {
            HTML::setPageTitle(['User Registration', 'Previously Verified']);
            HTML::setMetaDescription('This email address has been previously verified');
            Resources::addCSS('views.css');
            return response()->view('my.verification.previous', [
                'site_name' => FrameworkConfig::get('debear.names.site'),
            ], 400);
        }

        // Display a confirmation screen after recording the change.
        VerificationHelper::process($user);
        return redirect('/account/verified');
    }

    /**
     * Re-send the email verification request
     * @return JsonResponse A response of JSON data indicating success or failure
     */
    public function resend(): JsonResponse
    {
        // Must be logged in and not previously verified.
        $user = User::object();
        if (!isset($user) || !$user->isLoggedIn()) {
            return response()->json(['success' => false], 403); // Forbidden.
        } elseif ($user->isVerified()) {
            return response()->json(['success' => false], 409); // Conflict.
        }

        // Return as JSON object to the browser after procsesing.
        VerificationHelper::resendEmail($user);
        return response()->json(['success' => true]);
    }

    /**
     * Display a confirmation message for the verification process
     * @return Response|RedirectResponse The confirmation message to the user, an error message if not actually
     * verified yet or a redirect if not logged in
     */
    public function complete(): Response|RedirectResponse
    {
        if (!User::object()->isLoggedIn()) {
            // If the user is not logged in, send them back to the start of the registration process.
            return redirect('/register');
        } elseif (User::object()->isUnverified()) {
            // If the user is not verified, then we don't display a (generic) error message.
            return HTTP::sendForbidden(view('errors.403'));
        }

        // Some setup and then the view.
        HTML::setPageTitle(['User Registration', 'Email Verified']);
        HTML::setMetaDescription('Your email address has been verified for your '
            . FrameworkConfig::get('debear.names.site') . ' user account');
        Resources::addCSS('views.css');
        return response()
            ->view('my.verification.complete', [
                'site_name' => FrameworkConfig::get('debear.names.site'),
            ]);
    }
}
