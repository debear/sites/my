<?php

namespace DeBear\Http\Controllers\My;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Http\Modules\Forms;
use DeBear\Models\Skeleton\User;

class PasswordReset extends Controller
{
    /**
     * Validate the start of the request to reset a password
     * @param string $code The verification code passed in by the User to identify the attempt.
     * @return Response|RedirectResponse The relevant response, which could be a redirect if not a valid request
     */
    public function index(string $code): Response|RedirectResponse
    {
        // Validate the code and user.
        $user_id = Strings::validateUserTimeLock($code, FrameworkConfig::get('debear.login_details.timeout'));
        if (is_string($user_id)) {
            $user = User::where(
                [
                    ['user_id', '=', $user_id],
                    ['status', '!=', 'Disabled'],
                ]
            )->first();
        }

        if (!isset($user)) {
            return HTTP::sendBadRequest();
        }

        // Log the user in (logging out any existing user...).
        if (!User::object()->isLoggedIn()) {
            User::doLogin($user);
        } elseif (User::object()->user_id != $user->user_id) {
            User::doLogout();
            User::doLogin($user);
        }

        // Then take them to the appropriate form.
        return redirect('/account/reset');
    }

    /**
     * Form to reset a user's password
     * @return Response|RedirectResponse The relevant response, which could be a redirect if not a valid request
     */
    public function reset(): Response|RedirectResponse
    {
        // Process and render.
        $obj = new Forms('my/password_reset');
        return $obj->run();
    }
}
