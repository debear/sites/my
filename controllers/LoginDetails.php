<?php

namespace DeBear\Http\Controllers\My;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Http\Modules\Forms;
use DeBear\Helpers\HTTP;

class LoginDetails extends Controller
{
    /**
     * Form to action forgotten login details
     * @return Response|RedirectResponse The relevant response, which could be a redirect if not on
     * the current sub-domain
     */
    public function index(): Response|RedirectResponse
    {
        // If not on the correct sub-domain, perform the redirect.
        if (Request::server('SERVER_NAME') != FrameworkConfig::get('debear.url.subdomains.my')) {
            return redirect('https:' . HTTP::buildDomain('my') . Request::server('REQUEST_URI'));
        }

        // Process and render.
        $obj = new Forms('my/login_details');
        return $obj->run();
    }
}
