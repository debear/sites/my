<?php

return [
    // If there is no logged in user, send them to the Register page instead.
    'policies' => [
        'list' => 'logged_in',
        'redirect' => '/register',
    ],

    // Default values to load in to the form.
    'defaults' => [
        'user' => [
            'id',
        ],
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'user_id' => [
            'required' => true,
            'minlength' => 5,
            'maxlength' => 20,
            'custom' => [
                'is_unique' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validateUserID',
                    ],
                ],
            ],
        ],
        'password_curr' => [
            'required' => false,
            'minlength' => 8,
            'custom' => [
                'pwd_set' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validatePasswordFields',
                    ],
                    'args' => [
                        'curr' => 'password_curr',
                        'new' => 'password_new',
                        'mode' => 'curr',
                    ],
                ],
                'pwd_match' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validatePasswordCurrent',
                    ],
                ],
            ],
        ],
        'password_new1' => [
            'required' => false,
            'minlength' => 8,
            'custom' => [
                'pwd_set' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validatePasswordFields',
                    ],
                    'args' => [
                        'curr' => 'password_curr',
                        'new' => 'password_new',
                        'mode' => 'new',
                    ],
                ],
                'pwd_match' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validatePasswordMatch',
                    ],
                    'args' => [
                        'prefix' => 'password_new',
                    ],
                ],
                'pwd_policy' => [
                    'method' => [
                        '\DeBear\Helpers\User',
                        'validatePasswordPolicy'
                    ],
                    'args' => [
                        'check-reuse' => true,
                        'user_id' => 'user_id',
                        'email' => 'email',
                    ],
                ],
            ],
        ],
        'password_new2' => [
            'required' => false,
            'minlength' => 8,
        ],
        'email' => [
            'required' => true,
            'type' => 'email',
        ],
        'forename' => [
            'required' => true,
            'maxlength' => 15,
        ],
        'surname' => [
            'required' => true,
            'maxlength' => 20,
        ],
        'display_name' => [
            'required' => true,
            'maxlength' => 20,
        ],
        'dob' => [
            'required' => true,
            'type' => 'date',
            'min' => '1920-01-01',
            'max' => date('Y-m-d', strtotime('-4 year')),
        ],
        'timezone' => [
            'required' => true,
        ],
        'sec_code' => [
            'honeypot' => true,
            'required' => true,
            'maxlength' => 20,
        ],
    ],

    // Where and how to save the data.
    'mapping' => [
        'DeBear\Models\Skeleton\User' => [
            'key' => [
                'id',
            ],
            'form_fields' => [
                'id',
                'user_id',
                'forename',
                'surname',
                'password' => 'password_new1',
                'email',
                'display_name',
                'dob',
                'timezone',
            ],
        ],
    ],

    // What to do when we're at various stages.
    'actions' => [
        'success' => [
            // Record success in the activity table.
            ['DeBear\Http\Forms\My\Account', 'logSuccess'],
            // Send the user an email confirming (notifying?) the change.
            ['DeBear\Http\Forms\My\Account', 'confirmationEmail'],
        ],
        'failure' => [
            // Record a failure in the activity table.
            ['DeBear\Http\Forms\My\Account', 'logFailure'],
        ]
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/account',
        'message' => 'Changes to your account have been saved',
    ],
];
