@php
    // Page configuration
    HTML::setPageTitle('Account Details');
    HTML::setMetaDescription('Update your ' . FrameworkConfig::get('debear.names.site') . ' user account details');
    Resources::addCSS('skel/flags/16.css');
    Resources::addCSS('forms.css');
    Resources::addJS('skel/widgets/forms.js');
    Resources::addJS('account.js');
    Resources::addJS('password_policy.js');

    // Auto-fail the form if we find suspicious activity
    $form->validateSuspiciousActivity('account_update', 'user_id', 'Unable to verify details', [
        'user_id' => $form->getValue('user_id'),
        'email' => $form->getValue('email'),
    ]);
@endphp
<h1>{!! FrameworkConfig::get('debear.names.site') !!} Account Details</h1>

@if ($message = session('form_complete'))
    <div class="box success icon_valid">
        {!! $message !!}
    </div>
@endif

<fieldset class="section account">
    <legend>Account Information</legend>

    {{-- Username --}}
    @php
        $form->validateField('user_id');
    @endphp
    <ul class="inline_list field user_id clearfix">
        <li class="label {!! !$form->isFieldValid('user_id') ? 'error' : '' !!}" id="user_id_label">
            <label for="user_id">Username:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="user_id" name="user_id" value="{!! $form->getValue('user_id') !!}" autocomplete="username" {!! $form->getElementAttributes('user_id') !!} {!! $form->addTabindex() !!}>
        </li>
        <li class="status {!! $form->formFieldStatus('user_id') !!}" id="user_id_status"></li>
        <li class="info">Must be between 5 and 20 characters</li>
        <li class="error details {!! $form->isFieldValid('user_id') ? 'hidden' : '' !!}" id="user_id_error">{!! !$form->isFieldValid('user_id') ? $form->getFieldError('user_id') : '' !!}</li>
    </ul>

    {{-- Email --}}
    @php
        $form->validateField('email');
    @endphp
    <ul class="inline_list field email clearfix">
        <li class="label {!! !$form->isFieldValid('email') ? 'error' : '' !!}" id="email_label">
            <label for="email">Email Address:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="email" name="email" value="{!! $form->getValue('email') !!}" autocomplete="email" {!! $form->getElementAttributes('email') !!} {!! $form->addTabindex() !!} data-email="true">
        </li>
        <li class="status {!! $form->formFieldStatus('email') !!}" id="email_status"></li>
        <li class="info">Only used to contact you regarding use of the site</li>
        <li class="error details {!! $form->isFieldValid('email') ? 'hidden' : '' !!}" id="email_error">{!! !$form->isFieldValid('email') ? $form->getFieldError('email') : '' !!}</li>
    </ul>
</fieldset>

<fieldset class="section password">
    <legend>Password Update</legend>

    <p>This section is optional, and only required should you wish to change your password.</p>

    {{-- Current Password --}}
    @php
        $form->validateField('password_curr');
    @endphp
    <ul class="inline_list field password_curr clearfix">
        <li class="label {!! !$form->isFieldValid('password_curr') ? 'error' : '' !!}" id="password_curr_label">
            <label for="password_curr">Current:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="password" id="password_curr" name="password_curr" autocomplete="current-password" {!! $form->getElementAttributes('password_curr') !!} {!! $form->addTabindex() !!} data-status-field="password_curr">
        </li>
        <li class="status {!! $form->formFieldStatus('password_curr') !!}" id="password_curr_status"></li>
        <li class="info">Must be 8 or more characters</li>
        <li class="error details {!! $form->isFieldValid('password_curr') ? 'hidden' : '' !!}" id="password_curr_error">{!! !$form->isFieldValid('password_curr') ? $form->getFieldError('password_curr') : '' !!}</li>
    </ul>

    {{-- New Password --}}
    @php
        $form->validateField('password_new1');
        $form->validateField('password_new2');
    @endphp
    <ul class="inline_list field password_new1 clearfix">
        <li class="label {!! !$form->isFieldValid('password_new1') || !$form->isFieldValid('password_new2') ? 'error' : '' !!}" id="password_label">
            <label for="password_new1">New:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="password" id="password_new1" name="password_new1" autocomplete="new-password" {!! $form->getElementAttributes('password_new1') !!} {!! $form->addTabindex() !!} data-status-field="password">
        </li>
        <li class="status {!! $form->formFieldStatus('password_new1') !!}" id="password_status"></li>
        <li class="info"><a class="view-password-policy">View more about our password policy</a></li>
    </ul>

    {{-- Re-Enter Password --}}
    <ul class="inline_list field password_new2 clearfix">
        <li class="label {!! !$form->isFieldValid('password_new1') || !$form->isFieldValid('password_new2') ? 'error' : '' !!}" id="password_new2_label">
            <label for="password_new2">Re-enter:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="password" id="password_new2" name="password_new2" autocomplete="new-password" {!! $form->getElementAttributes('password_new2') !!} {!! $form->addTabindex() !!} data-status-field="password">
        </li>
        <li class="error details {!! $form->isFieldValid('password_new1') && $form->isFieldValid('password_new2') ? 'hidden' : '' !!}" id="password_error">{!! !$form->isFieldValid('password_new1') ? $form->getFieldError('password_new1') : (!$form->isFieldValid('password_new2') ? $form->getFieldError('password_new2') : '') !!}</li>
    </ul>
</fieldset>

<fieldset class="section user">
    <legend>User Information</legend>

    {{-- First Name --}}
    @php
        $form->validateField('forename');
    @endphp
    <ul class="inline_list field forename clearfix">
        <li class="label {!! !$form->isFieldValid('forename') ? 'error' : '' !!}" id="forename_label">
            <label for="forename">Forename:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="forename" name="forename" value="{!! $form->getValue('forename') !!}" autocomplete="given-name" {!! $form->getElementAttributes('forename') !!} {!! $form->addTabindex() !!}>
        </li>
        <li class="status {!! $form->formFieldStatus('forename') !!}" id="forename_status"></li>
        <li class="info">Must be no more than 15 characters</li>
        <li class="error details {!! $form->isFieldValid('forename') ? 'hidden' : '' !!}" id="forename_error">{!! !$form->isFieldValid('forename') ? $form->getFieldError('forename') : '' !!}</li>
    </ul>

    {{-- Surname --}}
    @php
        $form->validateField('surname');
    @endphp
    <ul class="inline_list field surname clearfix">
        <li class="label {!! !$form->isFieldValid('surname') ? 'error' : '' !!}" id="surname_label">
            <label for="surname">Surname:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="surname" name="surname" value="{!! $form->getValue('surname') !!}" autocomplete="family-name" {!! $form->getElementAttributes('surname') !!} {!! $form->addTabindex() !!}>
        </li>
        <li class="status {!! $form->formFieldStatus('surname') !!}" id="surname_status"></li>
        <li class="info">Must be no more than 20 characters</li>
        <li class="error details {!! $form->isFieldValid('surname') ? 'hidden' : '' !!}" id="surname_error">{!! !$form->isFieldValid('surname') ? $form->getFieldError('surname') : '' !!}</li>
    </ul>

    {{-- Display Name --}}
    @php
        $form->validateField('display_name');
    @endphp
    <ul class="inline_list field display_name clearfix">
        <li class="label {!! !$form->isFieldValid('display_name') ? 'error' : '' !!}" id="display_name_label">
            <label for="display_name">Display Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="display_name" name="display_name" value="{!! $form->getValue('display_name') !!}" autocomplete="nickname" {!! $form->getElementAttributes('display_name') !!} {!! $form->addTabindex() !!}>
        </li>
        <li class="status {!! $form->formFieldStatus('display_name') !!}" id="display_name_status"></li>
        <li class="info">Must be no more than 20 characters</li>
        <li class="error details {!! $form->isFieldValid('display_name') ? 'hidden' : '' !!}" id="display_name_error">{!! !$form->isFieldValid('display_name') ? $form->getFieldError('display_name') : '' !!}</li>
    </ul>

    @php
        $form->validateField('dob');
        $date_field = new DateField('dob', Arrays::merge(
            [
                'form' => $form,
                'value' => $form->getValue('dob'),
            ],
            $form->getElementValidation('dob')
        ));
    @endphp
    <ul class="inline_list field dob clearfix">
        <li class="label {!! !$form->isFieldValid('dob') ? 'error' : '' !!}" id="dob_label">
            <label for="dob">Date of Birth:</label>&nbsp;
        </li>
        <li class="value">
            {!! $date_field->render() !!}
        </li>
        <li class="status {!! $form->formFieldStatus('dob') !!}" id="dob_status"></li>
        <li class="info">Please enter in Day-Month-Year format</li>
        <li class="error details {!! $form->isFieldValid('dob') ? 'hidden' : '' !!}" id="dob_error">{!! !$form->isFieldValid('dob') ? $form->getFieldError('dob') : '' !!}</li>
    </ul>

    @php
        $tz = new Timezones(['geoip' => true]);
        $opt = [];
        foreach ($tz->getTimezones() as $code => $list) {
            // Form a row for the country
            $opt[] = ['label' => "<span class=\"flag flag16_{$list[0]->country_code}\">{$list[0]->country}</span>"];
            // Then each timezone
            foreach ($list as $tz)
                $opt[] = ['id' => $tz->timezone, 'label' => $tz->city];
        }
        $form->validateField('timezone', [ 'opt_list' => array_column($opt, 'id') ]);
        $dropdown = new Dropdown('timezone', $opt, Arrays::merge(
            [
                'form' => $form,
                'select' => '-- Please select your Nearest City --',
                'value' => $form->getValue('timezone'),
            ],
            $form->getElementValidation('timezone')
        ));
    @endphp
    <ul class="inline_list field timezone clearfix">
        <li class="label {!! !$form->isFieldValid('timezone') ? 'error' : '' !!}" id="timezone_label">
            <label for="timezone">Nearest City:</label>&nbsp;
        </li>
        <li class="value">
            {!! $dropdown->render() !!}
        </li>
        <li class="status {!! $form->formFieldStatus('timezone') !!}" id="timezone_status"></li>
        <li class="info">Used to establish the time where you are</li>
        <li class="error details {!! $form->isFieldValid('timezone') ? 'hidden' : '' !!}" id="timezone_error">{!! !$form->isFieldValid('timezone') ? $form->getFieldError('timezone') : '' !!}</li>
    </ul>
</fieldset>

<fieldset class="section security">
    <legend>Security Check</legend>

    @php
        $form->validateField('sec_code');
    @endphp
    <ul class="inline_list field sec_code clearfix">
        <li class="label {!! !$form->isFieldValid('sec_code') ? 'error' : '' !!}" id="sec_code_label">
            <label for="sec_code">So that we can confirm you are a real person trying to register a legitimate account, rather than a computer trying to abuse the system, please enter the code displayed below.</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="sec_code" name="sec_code" value="{!! $form->getValue('sec_code') !!}" {!! $form->getElementAttributes('sec_code') !!} {!! $form->addTabindex() !!}>
            {!! Strings::md5Code(time() * rand(), 8) !!}
        </li>
        <li class="status {!! $form->formFieldStatus('sec_code') !!}" id="sec_code_status"></li>
        <li class="error details {!! $form->isFieldValid('sec_code') ? 'hidden' : '' !!}" id="sec_code_error">{!! !$form->isFieldValid('sec_code') ? $form->getFieldError('sec_code') : '' !!}</li>
    </ul>
</fieldset>

<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, updating your account details...
    </div>
    <div class="box error icon_error {!! $form->validationPassed() ? 'hidden' : '' !!}">
      <strong>Unable to Update Account</strong>: There were errors when attempting to update the details of your user account. The fields marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before we can make these changes. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" {!! $form->addTabindex() !!}>Update Details</button>
</div>

{{-- Password Policy --}}
@include('my.account.password_policy', ['existing_user' => true])
