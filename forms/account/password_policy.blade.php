<modal class="password-policy hidden">
    <cover></cover>
    <fieldset class="status">
        <close>[ <a class="close-password-policy">Close</a> ]</close>
        <legend>The Password Policy at {!! FrameworkConfig::get('debear.names.site') !!}</legend>

        <p>Our password policy is designed to be straightforward for you to use, whilst still working to a high level of security for all our users. Passwords must:</p>
        <ol>
            <li>Be 8 or more characters in length.</li>
            @if ($existing_user)
                <li>Be different from your current password.</li>
            @endif
            <li>Not contain your Username.</li>
            <li>Not contain your Email Address.</li>
            <li>Not be a publicly exposed password from known data breaches &ndash; these are not considered secure passwords.</li>
        </ol>
        <p>We do not require you to use an unmemorable combination of upper case letters, lower case letters, numbers <em>and</em> special characters (such as ! or &amp;) &ndash; though you can still use any of these if you use a password managers... so long as it complies with the above rules. Essentially, we&#39;re big believers in <a href="https://xkcd.com/936/" target="_blank">this xkcd comic</a>.</p>

        <p>We also strongly advise you follow these actions, though these are considered good practice rather than being enforced in our policy (and not just to be applied here, but with <em>all</em> your user accounts):</p>
        <ol>
            <li>Use a different password to all your other user accounts.</li>
            <li>Consider using a Password Manager instead of writing down your passwords on paper.</li>
        </ol>
    </fieldset>
</div>
