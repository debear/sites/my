<?php

namespace DeBear\Http\Forms\My;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Policies;
use DeBear\Helpers\Comms\Email;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;

class Verification
{
    /**
     * Perform the act of verifying the user
     * @param User $user The user object of the user being verified.
     * @return void
     */
    public static function process(User $user): void
    {
        // Update the user's record.
        $user->status = 'Active';
        $user->account_verified = App::make(Time::class)->formatServer();
        $user->save();

        // Is this user the currently logged in user?
        if (!User::object()->isLoggedIn()) {
            User::doLogin($user);
        } elseif (User::object()->user_id != $user->user_id) {
            User::doLogout();
            User::doLogin($user);
        } else {
            Policies::recalc($user);
        }

        // Send a confirmation email.
        Email::send(
            'User: Verified',
            $user,
            [
                'type' => 'html',
                'reason' => 'register_verified',
                'send_time' => 'now',
                'send_unverified' => true, // Just to be sure...
            ]
        );
    }

    /**
     * Resend the verification email to a user
     * @param User $user The user object of the user requesting verification.
     * @return void
     */
    public static function resendEmail(User $user): void
    {
        Email::send(
            'User: Verification',
            $user,
            [
                'type' => 'html',
                'reason' => 'register_verify',
                'send_time' => 'now',
                'send_unverified' => true,
                'data' => [
                    'resend' => true,
                    'timeout' => FrameworkConfig::get('debear.verification.timeout'),
                ],
                'callback' => ['DeBear\Http\Forms\My\Register', 'emailVerification'],
            ]
        );
    }
}
