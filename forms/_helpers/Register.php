<?php

namespace DeBear\Http\Forms\My;

use Illuminate\Support\Facades\App;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Repositories\SuspiciousActivity;

class Register
{
    /**
     * Modify the data blob used in the User Verification email with in-line details
     * @param array $args The arguments we passed to the email processing. (Pass-by-Reference).
     * @return void
     */
    public static function emailVerification(array &$args): void
    {
        // Verification code.
        $args['data']['url'] = 'https:' . HTTP::buildDomain() . '/account/verify-' . Strings::generateUserTimeLock();
    }

    /**
     * Log a successful registration in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logSuccess(array $data): void
    {
        App::make(SuspiciousActivity::class)->record(
            'account_registration',
            'Successful Registration',
            false,
            [
                'user_id' => $data['raw']['proc']['user_id'],
                'email' => $data['raw']['proc']['email'],
            ]
        );
    }

    /**
     * Log a failed registration attempt in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logFailure(array $data): void
    {
        if (!isset($data['suspicious']) || !$data['suspicious']) {
            App::make(SuspiciousActivity::class)->record(
                'account_registration',
                'Failed Checks',
                false,
                [
                    'user_id' => $data['raw']['proc']['user_id'],
                    'email' => $data['raw']['proc']['email'],
                    'errors' => $data['err'],
                ]
            );
        }
    }
}
