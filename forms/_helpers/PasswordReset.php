<?php

namespace DeBear\Http\Forms\My;

use Illuminate\Support\Facades\App;
use DeBear\Helpers\Browser;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Policies;
use DeBear\Helpers\Comms\Email;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\GeoIP;
use DeBear\Repositories\SuspiciousActivity;

class PasswordReset
{
    /**
     * Log a successful registration in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logSuccess(array $data): void
    {
        // Flag an updated password.
        User::stampPasswordLogin();
        Policies::recalc(User::object());
        // Log the activity.
        App::make(SuspiciousActivity::class)->record(
            'account_password_reset',
            'Successful Password Reset',
            false,
            [
                'user_id' => User::object()->user_id,
            ]
        );
    }

    /**
     * Log a failed registration attempt in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logFailure(array $data): void
    {
        if (!isset($data['suspicious']) || !$data['suspicious']) {
            App::make(SuspiciousActivity::class)->record(
                'account_password_reset',
                'Failed Checks',
                false,
                [
                    'user_id' => User::object()->user_id,
                    'errors' => $data['err'],
                ]
            );
        }
    }

    /**
     * Resend the verification email to a user
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function confirmationEmail(array $data): void
    {
        Email::send(
            'User: Password Reset',
            User::object(),
            [
                'reason' => 'account_password_reset',
                'type' => 'html',
                'send_time' => 'now',
                'send_unverified' => true,
                'data' => [
                    'geoip_city' => App::make(GeoIP::class)->getCityFull(),
                    'geoip_country' => App::make(GeoIP::class)->getCountry(),
                    'geoip_country_name' => App::make(GeoIP::class)->getCountryName(),
                    'geoip_country_flag' => HTTP::buildStaticURLs('images', 'skel/flags/16/'
                        . strtolower(App::make(GeoIP::class)->getCountry()) . '.png'),
                    'browser' => Browser::get()->browser . ' on ' . Browser::get()->platform,
                ],
            ]
        );
    }
}
