<?php

namespace DeBear\Http\Forms\My;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Database\Eloquent\Collection;
use DeBear\Helpers\Browser;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Comms\Email;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\GeoIP;
use DeBear\Repositories\SuspiciousActivity;

class LoginDetails
{
    /**
     * Process the reset request (including determination)
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function processRequest(array $data): void
    {
        // How many user account(s) have this email address?
        $email = $data['raw']['proc']['email'];
        $users = User::where(
            [
                'email' => $email,
            ]
        )->get();
        if (!sizeof($users)) {
            // No match, so flag as an error.
            $code = 'unknown';
            $msg = 'Unknown email address';
            $data['err'][] = [
                'error' => $code,
                'message' => $msg,
                'all' => [
                    $code => $msg,
                ],
            ];
            static::logFailure($data);
            return;
        }

        // All good, so process.
        static::logSuccess($data);
        Email::send(
            'User: Reset Request',
            $email,
            [
                'reason' => 'account_reset_request',
                'type' => 'html',
                'send_time' => 'now',
                'send_unverified' => true,
                'data' => [
                    'timeout' => FrameworkConfig::get('debear.login_details.timeout'),
                    'body' => static::emailBody($users),
                    'geoip_city' => App::make(GeoIP::class)->getCityFull(),
                    'geoip_country' => App::make(GeoIP::class)->getCountry(),
                    'geoip_country_name' => App::make(GeoIP::class)->getCountryName(),
                    'geoip_country_flag' => HTTP::buildStaticURLs('images', 'skel/flags/16/'
                        . strtolower(App::make(GeoIP::class)->getCountry()) . '.png'),
                    'browser' => Browser::get()->browser . ' on ' . Browser::get()->platform,
                ],
            ]
        );
    }

    /**
     * Define the body of the email related to the user account(s) and the appropriate link(s)
     * @param Collection $users Collection of >= 1 user account linked to an email address.
     * @return string The body of the email to be sent
     */
    protected static function emailBody(Collection $users): string
    {
        if (sizeof($users) == 1) {
            // If one, display as a single link.
            $user_id = $users->first()->user_id;
            $reset_url = static::generateResetURL($user_id);
            return "<p>The password for your account <code>{$user_id}</code> can be <a href=\"$reset_url\">reset "
                . "at this link</a>.</p>";
        } else {
            // If multiple, display as individual links with username included.
            $ret = "<p>There are multiple user accounts linked to this email address, so if you wish to reset your "
                . "password please select the appropriate password reset link as each link is unique to only one "
                . "account:</p>\n\n<ul>";
            foreach ($users as $user) {
                $reset_url = static::generateResetURL($user->user_id);
                $ret .= "<li><code>{$user->user_id}</code>: <a href=\"$reset_url\">Reset Link</a></li>";
            }
            $ret .= '</ul>';
            return $ret;
        }
    }

    /**
     * Log a successful reset request in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logSuccess(array $data): void
    {
        App::make(SuspiciousActivity::class)->record(
            'account_reset_request',
            'Successful Reset Request',
            false,
            [
                'email' => $data['raw']['proc']['email'],
            ]
        );
    }

    /**
     * Log a failed reset request attempt in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logFailure(array $data): void
    {
        if (!isset($data['suspicious']) || !$data['suspicious']) {
            App::make(SuspiciousActivity::class)->record(
                'account_reset_request',
                'Failed Checks',
                false,
                [
                    'email' => $data['raw']['proc']['email'],
                    'errors' => $data['err'],
                ]
            );
        }
    }

    /**
     * Create the URL that is to be used for the password reset link
     * @param string $user_id The user_id we want to create a reset link for.
     * @return string The reset URL
     */
    protected static function generateResetURL(string $user_id): string
    {
        return 'https:' . HTTP::buildDomain() . '/account/reset-' . Strings::generateUserTimeLock($user_id);
    }
}
