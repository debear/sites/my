<?php

namespace DeBear\Http\Forms\My;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\Helpers\Browser;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Comms\Email;
use DeBear\Implementations\Hasher;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\GeoIP;
use DeBear\Repositories\SuspiciousActivity;

class Account
{
    /**
     * Ensure the requested User ID is allowed and not already taken
     * @param mixed $value User ID to check.
     * @param array $data  The full data passed to the form. (Optional).
     * @return string|boolean A bool of false if valid, otherwise an approrpiate error message
     */
    public static function validateUserID(mixed $value, array $data = []): string|bool
    {
        $curr_user_id = User::object()->user_id;
        if ($curr_user_id && ($value == $curr_user_id)) {
            // The current users (which is therefore allowed?).
            return false;
        } elseif (in_array($value, FrameworkConfig::get('debear.security.banned_ids'))) {
            // On a banned list?
            return 'This username is not allowed';
        } elseif (DB::table('USERS')->where('user_id', $value)->count()) {
            // If this user_id is already in use, return an error string.
            return 'This username is not allowed';
        }

        // Otherwise, a status boolean.
        return false;
    }

    /**
     * Validate the password entered matches the user's password... though it is optional
     * @param mixed $password Base password to check.
     * @param array $data     The full data passed to the form. (Optional).
     * @return string|boolean A bool of false if valid, otherwise an approrpiate error message
     */
    public static function validatePasswordCurrent(mixed $password, array $data = []): string|bool
    {
        if (!$password || static::checkUserPassword($data['user_id'], $password)) {
            return false;
        }
        return 'Your current password could not be verified';
    }

    /**
     * Worker method for checking the password matches what the user currently has
     * @param string $user_id  The ID of the user to be checked.
     * @param string $password The password being tested.
     * @return boolean Whether the password matches what we have on record for that user
     */
    protected static function checkUserPassword(string $user_id, string $password): bool
    {
        return App::make(Hasher::class)->check(
            $password,
            User::object()->password,
            [
                'salt' => $user_id,
            ]
        );
    }

    /**
     * Confirm the user has entered new passwords if they have entered their current password
     * @param mixed $value Base password.
     * @param array $data  The full data passed to the form.
     * @param array $opt   Run-time options.
     * @return string|boolean A bool of false if valid, otherwise an approrpiate error message
     */
    public static function validatePasswordFields(mixed $value, array $data, array $opt): string|bool
    {
        $pwd_curr = isset($data[$opt['curr']]) && is_string($data[$opt['curr']]) && $data[$opt['curr']];
        $pwd_new1 = isset($data[$opt['new'] . '1']) && is_string($data[$opt['new'] . '1']) && $data[$opt['new'] . '1'];
        $pwd_new2 = isset($data[$opt['new'] . '2']) && is_string($data[$opt['new'] . '2']) && $data[$opt['new'] . '2'];

        // If we have no current password but at least one entered new password, fail.
        if ($opt['mode'] == 'curr' && !$pwd_curr && ($pwd_new1 || $pwd_new2)) {
            return 'Please enter your current password';
        }
        // If we have a current password and at least one unentered new password, fail.
        if ($opt['mode'] == 'new' && $pwd_curr && (!$pwd_new1 || !$pwd_new2)) {
            return 'Please enter a new password';
        }

        // Otherwise, a status boolean.
        return false;
    }

    /**
     * Confirm the two entered passwords match
     * @param mixed $value Base password.
     * @param array $data  The full data passed to the form.
     * @param array $opt   Run-time options.
     * @return string|boolean A bool of false if valid, otherwise an approrpiate error message
     */
    public static function validatePasswordMatch(mixed $value, array $data, array $opt): string|bool
    {
        $pwd1 = $data[$opt['prefix'] . '1'] ?? false;
        $pwd2 = $data[$opt['prefix'] . '2'] ?? false;

        // If they don't match, return an error string.
        if ($pwd1 !== $pwd2) {
            return 'Please ensure the two passwords match';
        }

        // Otherwise, a status boolean.
        return false;
    }

    /**
     * Log a successful account update in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logSuccess(array $data): void
    {
        // Flag an updated password?
        if (isset($data['raw']['changes']['fields']['password_new1'])) {
            User::stampPasswordLogin();
        }
        // Log the activity.
        App::make(SuspiciousActivity::class)->record(
            'account_update',
            'Successful Account Update',
            false,
            [
                'user_id' => $data['raw']['proc']['user_id'],
            ]
        );
    }

    /**
     * Log a failed account update attempt in the activity log
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function logFailure(array $data): void
    {
        if (!isset($data['suspicious']) || !$data['suspicious']) {
            App::make(SuspiciousActivity::class)->record(
                'account_update',
                'Failed Checks',
                false,
                [
                    'user_id' => $data['raw']['proc']['user_id'],
                    'errors' => $data['err'],
                ]
            );
        }
    }

    /**
     * Resend the verification email to a user
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function confirmationEmail(array $data): void
    {
        // Only send the email if there were any changes made...
        if (!sizeof($data['raw']['changes']['fields'])) {
            return;
        }
        Email::send(
            'User: Account Updated',
            User::object(),
            [
                'reason' => 'account_update',
                'type' => 'html',
                'send_time' => 'now',
                'send_unverified' => true,
                'data' => [
                    'geoip_city' => App::make(GeoIP::class)->getCityFull(),
                    'geoip_country' => App::make(GeoIP::class)->getCountry(),
                    'geoip_country_name' => App::make(GeoIP::class)->getCountryName(),
                    'geoip_country_flag' => HTTP::buildStaticURLs('images', 'skel/flags/16/'
                        . strtolower(App::make(GeoIP::class)->getCountry()) . '.png'),
                    'browser' => Browser::get()->browser . ' on ' . Browser::get()->platform,
                ],
            ]
        );
    }
}
