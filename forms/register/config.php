<?php

return [
    // If the user is already logged in, send them to their My Account page instead.
    'policies' => [
        'list' => 'guest',
        'redirect' => '/account',
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'user_id' => [
            'required' => true,
            'minlength' => 5,
            'maxlength' => 20,
            'faker' => 'userName',
            'custom' => [
                'is_unique' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validateUserID',
                    ],
                ],
            ],
        ],
        'password1' => [
            'required' => true,
            'minlength' => 8,
            'faker' => [
                'bothify', [
                    str_repeat('*', 12),
                ],
            ],
            'custom' => [
                'pwd_match' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validatePasswordMatch',
                    ],
                    'args' => [
                        'prefix' => 'password',
                    ],
                ],
                'pwd_policy' => [
                    'method' => [
                        '\DeBear\Helpers\User',
                        'validatePasswordPolicy'
                    ],
                    'args' => [
                        'user_id' => 'user_id',
                        'email' => 'email',
                    ],
                ],
            ],
        ],
        'password2' => [
            'required' => true,
            'minlength' => 8,
            'faker' => [
                '_copy', 'password1',
            ],
        ],
        'email' => [
            'required' => true,
            'type' => 'email',
            'faker' => 'safeEmail',
        ],
        'forename' => [
            'required' => true,
            'maxlength' => 15,
            'faker' => 'firstName',
        ],
        'surname' => [
            'required' => true,
            'maxlength' => 20,
            'faker' => 'lastName',
        ],
        'display_name' => [
            'required' => true,
            'maxlength' => 20,
            'faker' => [
                '_copy', [
                    'forename',
                    'surname',
                ],
            ],
        ],
        'dob' => [
            'required' => true,
            'type' => 'date',
            'min' => '1920-01-01',
            'max' => date('Y-m-d', strtotime('-4 year')),
            'faker' => [
                'date', [
                    'Y-m-d',
                    '-4 year',
                ],
            ],
        ],
        'timezone' => [
            'required' => true,
            'faker' => 'timezone',
        ],
        'sec_code' => [
            'honeypot' => true,
            'required' => true,
            'maxlength' => 20,
            'faker' => [
                'bothify', [
                    str_repeat('*', 10),
                ],
            ],
        ],
        'terms' => [
            'required' => true,
            'type' => 'checkbox',
            'faker' => true,
        ],
    ],

    // Where and how to save the data.
    'mapping' => [
        'DeBear\Models\Skeleton\User' => [
            'key' => [
                'user_id',
            ],
            'form_fields' => [
                'user_id',
                'forename',
                'surname',
                'password' => 'password1',
                'email',
                'display_name',
                'dob',
                'timezone',
            ],
            'fixed_fields' => [
                'account_created' => [
                    'timestamp' => 'server_now',
                ],
                'status' => [
                    'string' => 'Unverified',
                ],
            ],
        ],
    ],

    // What to do when we're at various stages.
    'actions' => [
        'success' => [
            'login' => [
                'from_registration' => true,                   // Log the user in.
            ],
            ['DeBear\Http\Forms\My\Register', 'logSuccess'],   // Record success in the activity table.
            'email-reg' => [
                'name' => 'User: Registered',
                'reason' => 'register',
                'type' => 'html',
                'send_time' => 'now',
                'send_unverified' => true,
            ],
            'email-verif' => [
                'name' => 'User: Verification',
                'reason' => 'register_verify',
                'type' => 'html',
                'send_time' => '+5 minutes',
                'send_unverified' => true,
                'data' => [
                    'timeout' => FrameworkConfig::get('debear.verification.timeout'),
                ],
                'callback' => ['DeBear\Http\Forms\My\Register', 'emailVerification'],
            ],
        ],
        'failure' => [
            ['DeBear\Http\Forms\My\Register', 'logFailure'],   // Record a failure in the activity table.
        ]
    ],

    // Where to go upon completion.
    'next' => [
        'template' => 'complete',
    ],
];
