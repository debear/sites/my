@php
    // Page configuration
    HTML::setPageTitle(['User Registration', 'Complete']);
    HTML::setMetaDescription('Your new user account for the ' . FrameworkConfig::get('debear.names.site') . ' suite of sites has been created');
    Resources::addCSS('views.css');
@endphp

<h1>
    {!! FrameworkConfig::get('debear.names.site') !!} User Registration
</h1>
<div class="box success icon_valid">
    Your User Account has been created!
</div>
<p>
    Welcome! Your account has been created and you have been automatically logged in. However, we still ask that you verify your email address to confirm ownership and you will not receive any emails from us until this has been done, though this process does not prevent your ability to use the site.
</p>

<h3>
    What if I never receive the email?
</h3>
<p>
    We would expect you to receive the email within around 15 minutes, although in practice it should be almost instantaneous. In the first instance, please check your <em>Spam</em> or <em>Junk</em> folder(s), as emails from an unknown source may be stored there instead of your <em>Inbox</em>.
    If you haven&#39;t received the email after this timeframe, and have checked your <em>Spam</em> or <em>Junk</em> folder(s), then we suggest trying again by clicking the link at the top of the page, which we will continue to display until an email address has been verified. If you do not receive this email as well, we ask you email {!! HTML::buildEmailMarkup('registration') !!}, preferably from the email account you specified in the Registration form. Please include as many details as possible to assist our Support Team, but we will not be able to help you without knowing the User ID of the account you are attempting to create.
</p>

<fieldset class="section">
    <legend>Account Summary</legend>
    <dl class="summary">
        @php
            $summary = [
                'user_id' => 'User ID',
                'forename' => 'First Name',
                'surname' => 'Surname',
                'email' => 'E-Mail Address',
            ];
        @endphp
        @foreach ($summary as $field => $label)
            <dt>{!! $label !!}:</dt>
            <dd>{!! \DeBear\Models\Skeleton\User::object()->$field ?: '<em>Not Set</em>' !!}</dd>
        @endforeach
    </dl>
</fieldset>
