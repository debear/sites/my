@php
    // Page configuration
    HTML::setPageTitle(['Forgotten Details', 'Sent']);
    HTML::setMetaDescription('Information to recover forgotten or lost ' . FrameworkConfig::get('debear.names.site') . ' login details have been sent');
    Resources::addCSS('views.css');
@endphp

<h1>
    {!! FrameworkConfig::get('debear.names.site') !!} Login Details
</h1>
<div class="box success icon_valid">
    Login Details request processed!
</div>
<p>
    If the email address you entered matched that of a user in our system, we will have just sent it an email with your User ID and a link to reset your password.
</p>

<h3>
    That sounds a bit non-committal?
</h3>
<p>
    As unhelpful as it may appear, this is unfortunately intentional. Password reset systems are known way for attackers to gain unauthorised access to systems and this message is one of the many steps we take to try and keep ours secure &ndash; if we confirmed one way or another that an email address was attached to one of our users then that would provide a potential attacker information that will help focus their attack.
</p>

<h3>
    What if I never receive the email?
</h3>
<p>
    We would expect you to receive the email within around 15 minutes, although in practice it should be almost instantaneous. In the first instance, please check your <em>Spam</em> or <em>Junk</em> folder(s), as emails from an unknown source may be stored there instead of your <em>Inbox</em>.
    If you haven&#39;t received the email after this timeframe, and have checked your <em>Spam</em> or <em>Junk</em> folder(s), then try to request another email in a short while in case there was a temporary problem getting the email to you. If this email also fails then please email {!! HTML::buildEmailMarkup('registration') !!}, preferably from the appropriate email account and our Support Team will try to assist you.
</p>
