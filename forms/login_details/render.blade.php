@php
    // Page configuration
    HTML::setPageTitle('Forgotten Details');
    HTML::setMetaDescription('Request information to recover your forgotten or lost login details to  ' . FrameworkConfig::get('debear.names.site'));
    Resources::addCSS('forms.css');
    Resources::addJS('skel/widgets/forms.js');

    // Auto-fail the form if we find suspicious activity
    $form->validateSuspiciousActivity('account_reset_request', 'email', 'Unable to verify details', [
        'email' => $form->getValue('email'),
    ]);
@endphp

<h1>{!! FrameworkConfig::get('debear.names.site') !!} Login Details</h1>

<p class="narrow">
    Enter your email address below and we'll email you a link to reset your password if you need to change it, along with your user ID if you&#39;ve forgotten it. Don&#39;t worry, no changes will be made to your account until you complete the reset process in the email.<br>
</p>
<p class="narrow">
    <em>If it helps with a forgotten password, we only asked it was at least 8 characters &ndash; there is no restriction forcing you to have used upper case letters, numbers or punctuation symbols (like !, &amp;, or $) unless you chose to do so.</em>
</p>

<fieldset class="section details">
    {{-- Email --}}
    @php
        $form->validateField('email');
    @endphp
    <ul class="inline_list field email clearfix">
        <li class="label {!! !$form->isFieldValid('email') ? 'error' : '' !!}" id="email_label">
            <label for="email">Email Address:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="email" name="email" value="{!! $form->getValue('email') !!}" autocomplete="email" {!! $form->getElementAttributes('email') !!} {!! $form->addTabindex() !!} data-email="true">
        </li>
        <li class="status {!! $form->formFieldStatus('email') !!}" id="email_status"></li>
        <li class="error details {!! $form->isFieldValid('email') ? 'hidden' : '' !!}" id="email_error">{!! !$form->isFieldValid('email') ? $form->getFieldError('email') : '' !!}</li>
    </ul>
</fieldset>

<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, sending email...
    </div>
    <div class="box error icon_error {!! $form->validationPassed() ? 'hidden' : '' !!}">
      <strong>Unable to process request</strong>: There were errors when attempting to process your login details request. The fields marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before we can continue. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" {!! $form->addTabindex() !!}>Send Email</button>
</div>
