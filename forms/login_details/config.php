<?php

return [
    // If the user is already logged in, send them to their My Account page instead.
    'policies' => [
        'list' => 'guest',
        'redirect' => '/account',
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'email' => [
            'required' => true,
            'type' => 'email',
            'faker' => 'safeEmail',
        ],
    ],

    // What to do when we're at various stages.
    'actions' => [
        'success' => [
            // Processing is done within the helper class.
            ['DeBear\Http\Forms\My\LoginDetails', 'processRequest'],
        ],
        'failure' => [
            // Record a failure in the activity table.
            ['DeBear\Http\Forms\My\LoginDetails', 'logFailure'],
        ]
    ],

    // Where to go upon completion.
    'next' => [
        'template' => 'sent',
    ],
];
