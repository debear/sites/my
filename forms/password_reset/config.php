<?php

return [
    // If there is no logged in user, send them to start of the process to recover details.
    'policies' => [
        'list' => 'logged_in',
        'redirect' => '/login-details',
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'password1' => [
            'required' => true,
            'minlength' => 8,
            'faker' => [
                'bothify', [
                    str_repeat('*', 12),
                ],
            ],
            'custom' => [
                'pwd_match' => [
                    'method' => [
                        '\DeBear\Http\Forms\My\Account',
                        'validatePasswordMatch',
                    ],
                    'args' => [
                        'prefix' => 'password',
                    ],
                ],
                'pwd_policy' => [
                    'method' => [
                        '\DeBear\Helpers\User',
                        'validatePasswordPolicy'
                    ],
                    'args' => [
                        'user_id' => 'user_id',
                        'email' => 'email',
                    ],
                ],
            ],
        ],
        'password2' => [
            'required' => true,
            'minlength' => 8,
            'faker' => [
                '_copy', 'password1',
            ],
        ],
    ],

    // Where and how to save the data.
    'mapping' => [
        'DeBear\Models\Skeleton\User' => [
            'key' => [
                'user_id',
            ],
            'form_fields' => [
                'password' => 'password1',
            ],
            'user_fields' => [
                'user_id',
            ],
        ],
    ],

    // What to do when we're at various stages.
    'actions' => [
        'success' => [
            // Record success in the activity table.
            ['DeBear\Http\Forms\My\PasswordReset', 'logSuccess'],
            // Send the user an email confirming (notifying?) the change.
            ['DeBear\Http\Forms\My\PasswordReset', 'confirmationEmail'],
        ],
        'failure' => [
            // Record a failure in the activity table.
            ['DeBear\Http\Forms\My\PasswordReset', 'logFailure'],
        ]
    ],

    // Where to go upon completion.
    'next' => [
        'template' => 'complete',
    ],
];
