@php
    // Page configuration
    HTML::setPageTitle('Password Reset');
    HTML::setMetaDescription('Reset the password to your ' . FrameworkConfig::get('debear.names.site') . ' user account');
    Resources::addCSS('forms.css');
    Resources::addJS('skel/widgets/forms.js');
    Resources::addJS('password_reset.js');
    Resources::addJS('password_policy.js');

    // Auto-fail the form if we find suspicious activity
    $form->validateSuspiciousActivity('account_password_reset', 'user_id', 'Unable to verify details', [
        'user_id' => \DeBear\Models\Skeleton\User::object()->user_id,
    ]);
@endphp
<h1>{!! FrameworkConfig::get('debear.names.site') !!} Password Reset</h1>

@if (Policies::match('state:password:fails-policy'))
    <div class="box error icon_error">
        We have recently introduced a new password policy to improve the security on the platform, but we have determined that your
        password does not conform to this policy. <strong>Your password has not been exposed</strong>, we are simply asking you to
        change your password to something stronger and more secure for your security and the security of all our users.
    </div>
@endif

<p class="narrow">
    Please enter your new password in the form below. We do not force you to enter a combination of numbers, punctuation and upper / lower case letters, only that your password is a minimum of 8 characters, does not include your Username or Email Address and that it isn't classed as "insecure" by being a common password that has been exposed in various data breaches.
</p>

<fieldset class="section account">
    {{-- User ID (hidden, for password managers?) --}}
    <input type="text" class="hidden" id="user_id" name="user_id" value="{!! \DeBear\Models\Skeleton\User::object()->user_id !!}" autocomplete="username">

    {{-- Password --}}
    @php
        $form->validateField('password1');
        $form->validateField('password2');
    @endphp
    <ul class="inline_list field password1 clearfix">
        <li class="label {!! !$form->isFieldValid('password1') ? 'error' : '' !!}" id="password_label">
            <label for="password1">New Password:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="password" id="password1" name="password1" value="{!! $form->getValue('password1') !!}" autocomplete="new-password" {!! $form->getElementAttributes('password1') !!} {!! $form->addTabindex() !!} data-status-field="password">
        </li>
        <li class="info">Must be 8 or more characters</li>
        <li class="status {!! $form->formFieldStatus('password1') !!}" id="password_status"></li>
    </ul>

    {{-- Re-Enter Password --}}
    <ul class="inline_list field password2 clearfix">
        <li class="label {!! !$form->isFieldValid('password1') ? 'error' : '' !!}" id="password2_label">
            <label for="password2">Re-enter:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="password" id="password2" name="password2" value="{!! $form->getValue('password2') !!}" autocomplete="new-password" {!! $form->getElementAttributes('password2') !!} {!! $form->addTabindex() !!} data-status-field="password">
        </li>
        <li class="info no-status"><a class="view-password-policy">View more about our password policy</a></li>
        <li class="error details {!! $form->isFieldValid('password1') ? 'hidden' : '' !!}" id="password_error">{!! !$form->isFieldValid('password1') ? $form->getFieldError('password1') : '' !!}</li>
    </ul>
</fieldset>

<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, updating your password...
    </div>
    <div class="box error icon_error {!! $form->validationPassed() ? 'hidden' : '' !!}">
      <strong>Unable to update password</strong>: There were errors when attempting to changee your password. The fields marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before we can proceed. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" {!! $form->addTabindex() !!}>Reset Password</button>
</div>

{{-- Password Policy --}}
@include('my.account.password_policy', ['existing_user' => false])
