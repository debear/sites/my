@php
    // Page configuration
    HTML::setPageTitle(['Password Reset', 'Complete']);
    HTML::setMetaDescription('The password to your ' . FrameworkConfig::get('debear.names.site') . ' user account has been reset');
    Resources::addCSS('views.css');
@endphp

<h1>
    {!! FrameworkConfig::get('debear.names.site') !!} Password Reset
</h1>
<div class="box success icon_valid">
    Your password has been reset!
</div>
<p>
    This change will take immediate effect, though as you are already logged in in practice this means you will only need it next time you wish to log in. If you wish to make further changes to your {!! FrameworkConfig::get('debear.names.site') !!} user account please use the <a href="/account">My Account</a> age.
</p>
